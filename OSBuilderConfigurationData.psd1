# Configuration Defaults for OSBuilder
@{
    OSBuilderPath = "C:\OSBuilder";

    #======================================================================================
    #   Set Primary Paths 18.10.1
    #======================================================================================
    OSBuilderContent = "$($OSBuilderPath)\Content";
    # OSBuilderOSBuilds = $(Join-Path $OSBuilderPath "OSBuilds");
    # OSBuilderOSMedia = $(Join-Path $OSBuilderPath "OSMedia");
    # OSBuilderTasks = $(Join-Path $OSBuilderPath "Tasks");

    # #======================================================================================
    # #   Set Local Catalog 18.10.1
    # #======================================================================================
    # CatalogLocal = $(Join-Path $OSBuilderContent "Updates\Cat.json");

    # #======================================================================================
    # #   OSBuilder URLs 18.10.2
    # #======================================================================================
    # OSBuilderURL = "https://raw.githubusercontent.com/OSDeploy/OSBuilder.Public/master/OSBuilder.json";
    # OSBuilderCatalogURL = "https://raw.githubusercontent.com/OSDeploy/OSBuilder.Public/master/Content/Updates/Cat.json";

    # #======================================================================================
    # #   Create Paths 18.10.1
    # #======================================================================================
    # OSBuilderStructure = @{
    #     $OSBuilderContent = @(
    #         "Drivers",
    #         "ExtraFiles",
    #         "Mount",
    #         "MVLS",
    #         "Packages",
    #         "Provisioning",
    #         "Scripts",
    #         "StartLayout",
    #         "Unattend",
    #         "Updates",
    #         "Updates\Custom",
    #         "WinPE",
    #         "WinPE\ADK",
    #         "WinPE\DaRT",
    #         "WinPE\Drivers",
    #         "WinPE\ExtraFiles",
    #         "WinPE\Scripts"
    #     );
    # }         
}