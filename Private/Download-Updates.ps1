function Download-Updates
{
    <#
    .SYNOPSIS
        Check or Download All Updates in the given Update Catalog if they do not exist.
    
    .PARAMETER UpdateCatalog
        A list of applicable updates to the current OS version and edition which has been filtered from the catalog XML source.

    .PARAMETER UpdateCachePath
        A local path to cache update downloads, by convention this is C:\OSBuilder\Content\Updates

    .PARAMETER DownloadUpdates
        Omit this switch to perform a dry-run / check for applicable updates without downloading them.
    #>
    [CmdletBinding()]
    Param(
        [parameter(Mandatory=$true,ValueFromPipeline=$true)]
        $UpdateCatalog,


        [string] $UpdateCachePath,

        [Boolean] $DownloadUpdates
    )

    $Execute = $true

    foreach ($Update in $UpdateCatalog) {
        $(Get-ChildItem -Path $UpdateCachePath -Directory -Recurse | Where-Object {$_.Name -eq $($Update.KBTitle)}).FullName
        if (!(Test-Path "$UpdateCachePath\*\$($Update.KBTitle)\$($Update.FileName)")) {
            if ($DownloadUpdates) {
                Write-Warning "Missing $($Update.KBTitle) ... Downloading"
                Get-OSBuilderUpdates -FilterKBTitle "$($Update.KBTitle)" -Download -HideDetails
            } else {
                Write-Warning "Missing $($Update.KBTitle) ... Execution will be Disabled"
                $Execute = $false
            }
        }
    }
}