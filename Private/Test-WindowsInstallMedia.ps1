function Test-WindowsInstallMedia
{
	<#
	.SYNOPSIS
		Test whether the path given is a windows installation media ISO
	.DESCRIPTION
		Mounts an .ISO file and tests for the presence of install.wim.
		Mainly used within a Where-Object cmdlet to filter out installation media from other ISO files.
	.PARAMETER Path
		Specify the full path to an .ISO file to test
	.OUTPUTS
		Boolean $true or $false
	#>
	[CmdletBinding()]
	Param(
		[parameter(Mandatory=$true, HelpMessage="Enter the full path to an .iso file to check whether it is windows installation media.")]
		[string] $Path
	)

	Write-Verbose -Message "Mounting .iso at $($Path)"
	$MountedImage = Mount-DiskImage -ImagePath $Path -Access ReadOnly -PassThru

	$MountedVolume = $MountedImage | Get-Volume
	Write-Verbose "Drive Letter $($MountedVolume.DriveLetter)"
	$Result = Test-Path -Path (Join-Path -Path "$($MountedVolume.DriveLetter):\sources" "install.wim")

	Write-Verbose -Message "Unmounting .iso at $($Path)"
	$MountedImage | Dismount-DiskImage 

	return $Result
}
