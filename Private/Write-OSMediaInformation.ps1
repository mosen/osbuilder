function Write-OSMediaInformation
{
    <#
    .SYNOPSIS
        Write information about an OSMedia directory to the console.
    #>
    [CmdletBinding()]
    Param(
        [parameter(Mandatory=$true,ValueFromPipeline=$true)]
        $WindowsImage
    )

    $OSImageName = $($WindowsImage.ImageName)
    $OSImageDescription = $($WindowsImage.ImageDescription)
    $OSArchitecture = Format-WindowsImageArchitecture $WindowsImage.Architecture
    $OSEditionID = $($WindowsImage.EditionId)
    $OSInstallationType = $($WindowsImage.InstallationType)
    $OSLanguages = $($WindowsImage.Languages)
    $OSBuild = $($WindowsImage.Build)
    $OSVersion = $($WindowsImage.Version)
    $OSSPBuild = $($WindowsImage.SPBuild)
    $OSSPLevel = $($WindowsImage.SPLevel)
    $OSImageBootable = $($WindowsImage.ImageBootable)
    $OSWIMBoot = $($WindowsImage.WIMBoot)
    $OSCreatedTime = $($WindowsImage.CreatedTime)
    $OSModifiedTime = $($WindowsImage.ModifiedTime)
    #======================================================================================
    Write-Host "OSMedia Information" -ForegroundColor Yellow
    Write-Host "-Source Path:           $OSSourcePath" -ForegroundColor Cyan
    Write-Host "-Image File:            $OSImagePath" -ForegroundColor Cyan
    Write-Host "-Image Index:           $OSImageIndex" -ForegroundColor Cyan
    Write-Host "-Name:                  $OSImageName" -ForegroundColor Cyan
    Write-Host "-Description:           $OSImageDescription" -ForegroundColor Cyan
    Write-Host "-Architecture:          $OSArchitecture" -ForegroundColor Cyan
    Write-Host "-Edition:               $OSEditionID" -ForegroundColor Cyan
    Write-Host "-Type:                  $OSInstallationType" -ForegroundColor Cyan
    Write-Host "-Languages:             $OSLanguages" -ForegroundColor Cyan
    Write-Host "-Build:                 $OSBuild" -ForegroundColor Cyan
    Write-Host "-Version:               $OSVersion" -ForegroundColor Cyan
    Write-Host "-SPBuild:               $OSSPBuild" -ForegroundColor Cyan
    Write-Host "-SPLevel:               $OSSPLevel" -ForegroundColor Cyan
    Write-Host "-Bootable:              $OSImageBootable" -ForegroundColor Cyan
    Write-Host "-WimBoot:               $OSWIMBoot" -ForegroundColor Cyan
    Write-Host "-Created Time:          $OSCreatedTime" -ForegroundColor Cyan
    Write-Host "-Modified Time:         $OSModifiedTime" -ForegroundColor Cyan
    #======================================================================================
}