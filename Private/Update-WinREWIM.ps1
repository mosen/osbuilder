function Update-WinREWIM
{
    <#
    .DESCRIPTION
        This cmdlet contains only the Update-OSMedia steps regarding the winre.wim file
    
    .PARAMETER LogPath
        Output path for logs. This was conventionally the PELogs path aka WinPE\info\logs
    .PARAMETER UpdateCatServicing
        The Servicing Stack Update Catalog, Downloaded from the OSBuilder.Public Repo.
    .PARAMETER UpdateCatCumulative
        The Cumulative Update Catalog, Downloaded from the OSBuilder.Public Repo.
    #>
    [CmdletBinding()]
    Param(
        [parameter(Mandatory=$true)]
        [string] $ImagePath,

        [parameter(Mandatory=$true)]
        [string] $OSBuilderContent,

        [parameter()]
        [string] $LogPath,

        [switch]$CustomCumulativeUpdate,
        [switch]$CustomServicingStack,

        $UpdateCatServicing,
        $UpdateCatCumulative
    )

    $MountWinRE = Join-Path $OSBuilderContent\Mount "winre$((Get-Date).ToString('mmss'))"
    if ( ! (Test-Path "$MountWinRE")) {New-Item "$MountWinRE" -ItemType Directory -Force | Out-Null}

    Write-Host "===========================================================================" -ForegroundColor Yellow
    Write-Host "WinPE Phase (winre.wim): Mount WinRE WIM" -ForegroundColor Yellow
    Mount-WindowsImage -ImagePath "$WimTemp\winre.wim" -Index 1 -Path "$MountWinRE" -Optimize -LogPath "$PELogs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Mount-WindowsImage-winre.wim.log"

    Write-Host "===========================================================================" -ForegroundColor Yellow
    Write-Host "WinPE Phase (winre.wim): Servicing Update" -ForegroundColor Yellow
    if (!($null -eq $UpdateCatServicing)) {
        if ($CustomServicingStack.IsPresent) {
            Write-Host "winre.wim:"
            Add-WindowsPackage -Path "$MountWinRE" -PackagePath "$($UpdateCatServicing.FullName)" -LogPath "$PELogs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-UpdateServicing-winre.wim.log"
        } else {
            foreach ($Update in $UpdateCatServicing) {
                $UpdateSSU = $(Get-ChildItem -Path $OSBuilderContent\Updates -Directory -Recurse | Where-Object {$_.Name -eq $($Update.KBTitle)}).FullName
                if (Test-Path "$UpdateSSU") {
                    Write-Host "winre.wim: $UpdateSSU"
                    if (Get-WindowsPackage -Path "$MountWinRE" | Where-Object {$_.PackageName -like "*$($Update.KBNumber)*"}) {
                        Write-Warning "KB$($Update.KBNumber) Installed ... Skipping Update"
                    } else {
                        Add-WindowsPackage -Path "$MountWinRE" -PackagePath "$UpdateSSU" -LogPath "$PELogs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-UpdateServicing-KB$($Update.KBNumber)-winre.wim.log"
                    }
                } else {
                    Write-Warning "Not Found: $UpdateSSU"
                }
            }
        }
    }

    Write-Host "===========================================================================" -ForegroundColor Yellow
    Write-Host "WinPE Phase (winre.wim): Cumulative Update" -ForegroundColor Yellow
    if (!($null -eq $UpdateCatCumulative)) {
        if ($CustomCumulativeUpdate.IsPresent) {
            Write-Host "winre.wim:"
            Add-WindowsPackage -Path "$MountWinRE" -PackagePath "$($UpdateCatCumulative.FullName)" -LogPath "$PELogs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-UpdateCumulative-winre.wim.log"
            Dism /Image:"$MountWinRE" /Cleanup-Image /StartComponentCleanup /ResetBase /LogPath:"$PELogs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Dism-Cleanup-Image-winre.wim.log"
        } else {
            foreach ($Update in $UpdateCatCumulative) {
                $UpdateCU = $(Get-ChildItem -Path $OSBuilderContent\Updates -Directory -Recurse | Where-Object {$_.Name -eq $($Update.KBTitle)}).FullName
                if (Test-Path "$UpdateCU") {
                    Write-Host "winre.wim: $UpdateCU"
                    #if (Get-WindowsPackage -Path "$MountWinRE" | Where-Object {$_.PackageName -like "*$($Update.KBNumber)*"}) {}
                    $SessionsXmlWinRE = "$MountWinRE\Windows\Servicing\Sessions\Sessions.xml"
                    if (Test-Path $SessionsXmlWinRE) {
                        [xml]$XmlDocument = Get-Content -Path $SessionsXmlWinRE
                        if ($XmlDocument.Sessions.Session.Tasks.Phase.package | Where-Object {$_.Name -like "*$($Update.KBNumber)*" -and $_.targetState -eq 'Installed'}) {
                            Write-Warning "KB$($Update.KBNumber) Installed ... Skipping Update"
                        } else {
                            Add-WindowsPackage -Path "$MountWinRE" -PackagePath "$UpdateCU" -LogPath "$PELogs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-UpdateCumulative-KB$($Update.KBNumber)-winre.wim.log"
                            Dism /Image:"$MountWinRE" /Cleanup-Image /StartComponentCleanup /ResetBase /LogPath:"$PELogs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Dism-Cleanup-Image-winre.wim.log"
                        }
                    } else {
                        Add-WindowsPackage -Path "$MountWinRE" -PackagePath "$UpdateCU" -LogPath "$PELogs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-UpdateCumulative-KB$($Update.KBNumber)-winre.wim.log"
                        Dism /Image:"$MountWinRE" /Cleanup-Image /StartComponentCleanup /ResetBase /LogPath:"$PELogs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Dism-Cleanup-Image-winre.wim.log"
                    }
                } else {
                    Write-Warning "Not Found: $UpdateCU"
                }
            }
        }
    }

    Write-Host "===========================================================================" -ForegroundColor Yellow
    Write-Host "WinPE Phase (winre.wim): Exporting Package Inventory" -ForegroundColor Yellow
    Write-Host "$PEInfo\winre-WindowsPackage.txt"
    Export-WindowsPackageInformation -MountPath $MountWinRE -OutPath $PEInfo -Prefix "winre"

    Write-Host "===========================================================================" -ForegroundColor Yellow
    Write-Host "WinPE Phase (winre.wim): Dismount and Save" -ForegroundColor Yellow
    Write-Host "winre.wim: Dismount and Save $MountWinRE"
    Dismount-WindowsImage -Path "$MountWinRE" -Save -LogPath "$PELogs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Dismount-WindowsImage-winre.wim.log" | Out-Null

    Write-Host "===========================================================================" -ForegroundColor Yellow
    Write-Host "WinPE Phase (winre.wim): Exporting WinPE WIMs" -ForegroundColor Yellow
    Write-Host "winre.wim: Exporting to $WinPE\winre.wim"
    Export-WindowsImage -SourceImagePath "$WimTemp\winre.wim" -SourceIndex 1 -DestinationImagePath "$WinPE\winre.wim" -LogPath "$PELogs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Export-WindowsImage-winre.wim.log" | Out-Null

    if (Test-Path "$MountWinRE") {Remove-Item -Path "$MountWinRE" -Force -Recurse | Out-Null}
}