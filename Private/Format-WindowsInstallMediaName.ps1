function Format-WindowsInstallMediaName
{
	<#
	.SYNOPSIS
        Format a windows installer media into a predictable directory naming format.

    .DESCRIPTION
        This function takes information about installer media (in the format given by Get-WindowsImage) and generates a predictable folder name. The naming scheme is identical
        to the OSBuilder main branch.

    .PARAMETER WindowsImage
        Windows image information previously extracted using `Get-WindowsImage -ImagePath -Index`

    .PARAMETER RegCurrentVersion
        Information extracted from the installer media registry. The installer media would have been mounted to retrieve this information.

    .EXAMPLE
        $img = Get-WindowsImage -ImagePath D:\sources\install.wim -Index 6
        ... load online mounted registry CurrentVersion key ...
        Format-WindowsInstallMediaName -WindowsImage $img -RegCurrentVersion $cv

        Win10 Edu x64 1709 en-GB 16299.15
	#>
	[CmdletBinding()]
	Param(
        [parameter(Mandatory=$true, ValueFromPipeline=$true)]
        [Microsoft.Dism.Commands.ImageInfoObject] $WindowsImage,

        [parameter(ParameterSetName='RegistryObject')]
        $RegCurrentVersion,

        [parameter(ParameterSetName='RegistryFile')]
        $CurrentVersionXMLPath
    )

    $ImageName = $($WindowsImage.ImageName)
	$ImageName = $ImageName -replace "Windows 10", "Win10"
	$ImageName = $ImageName -replace "Enterprise", "Ent"
	$ImageName = $ImageName -replace "Education", "Edu"
	$ImageName = $ImageName -replace " for ", " "
	$ImageName = $ImageName -replace "Workstations", "Wks"
	$ImageName = $ImageName -replace "Windows Server 2016", "Svr2016"
	$ImageName = $ImageName -replace "ServerStandardACore", "Std Core"
	$ImageName = $ImageName -replace "ServerDatacenterACore", "DC Core"
	$ImageName = $ImageName -replace "ServerStandardCore", "Std Core"
	$ImageName = $ImageName -replace "ServerDatacenterCore", "DC Core"
	$ImageName = $ImageName -replace "ServerStandard", "Std"
	$ImageName = $ImageName -replace "ServerDatacenter", "DC"
	$ImageName = $ImageName -replace "Standard", "Std"
	$ImageName = $ImageName -replace "Datacenter", "DC"
	$ImageName = $ImageName -replace 'Desktop Experience', 'DTE'
	$ImageName = $ImageName -replace '\(', ''
	$ImageName = $ImageName -replace '\)', ''

    $OSArchitecture = Format-WindowsImageArchitecture $($WindowsImage.Architecture)
    $OSVersionNumber = $null
    $OSVersionNumber = $($RegCurrentVersion.ReleaseId)

    if ($RegCurrentVersion -eq $null) {
        $RegCurrentVersion = Import-Clixml -Path "$CurrentVersionXMLPath"
    }
    # if (Test-Path "$LogsXML\CurrentVersion.xml") {
    #     $RegCurrentVersion = Import-Clixml -Path "$LogsXML\CurrentVersion.xml"
    #     $OSVersionNumber = $($RegCurrentVersion.ReleaseId)
    # } else {
    #     if ($OSBuild -eq 10240) {$OSVersionNumber = 1507}
    #     if ($OSBuild -eq 14393) {$OSVersionNumber = 1607}
    #     if ($OSBuild -eq 15063) {$OSVersionNumber = 1703}
    #     if ($OSBuild -eq 16299) {$OSVersionNumber = 1709}
    #     if ($OSBuild -eq 17134) {$OSVersionNumber = 1803}
    # }
    $OSLanguages = $($WindowsImage.Languages)
    $RegCurrentVersionUBR = $($RegCurrentVersion.UBR)
    $OSBuild = $($WindowsImage.Build)
    $UBR = "$OSBuild.$RegCurrentVersionUBR"

    $InstallMediaName = "$ImageName $OSArchitecture $OSVersionNumber $OSLanguages $UBR"
    return $InstallMediaName
}
