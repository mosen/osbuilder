function Format-WindowsImageArchitecture
{
	Param($Architecture)
	Switch ($Architecture) {
		0 { Return 'x86' }
		1 { Return 'MIPS' }
		2 { Return 'Alpha' }
		3 { Return 'PowerPC' }
		6 { Return 'ia64' }
		9 { Return 'x64' } 
		default { Return $null }
	}
}