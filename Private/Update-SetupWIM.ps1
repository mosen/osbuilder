function Update-SetupWIM
{
    <#
    .DESCRIPTION
        This cmdlet contains only the Update-OSMedia steps regarding the setup.wim file
    
    .PARAMETER ImagePath
        Path to the .wim to mount
    .PARAMETER LogPath
        Output path for logs. This was conventionally the PELogs path aka WinPE\info\logs
    .PARAMETER UpdateCatServicing
        The Servicing Stack Update Catalog, Downloaded from the OSBuilder.Public Repo.
    .PARAMETER UpdateCatCumulative
        The Cumulative Update Catalog, Downloaded from the OSBuilder.Public Repo.
    #>
    [CmdletBinding()]
    Param(
        [parameter(Mandatory=$true)]
        [string] $ImagePath,

        [parameter(Mandatory=$true)]
        [string] $OSBuilderContent,

        [parameter()]
        [string] $LogPath,

        [switch]$CustomCumulativeUpdate,
        [switch]$CustomServicingStack,

        $UpdateCatServicing,
        $UpdateCatCumulative
    )

    $MountSetup = Join-Path $OSBuilderContent\Mount "setup$((Get-Date).ToString('mmss'))"
    if ( ! (Test-Path "$MountSetup")) {New-Item "$MountSetup" -ItemType Directory -Force | Out-Null}

    Write-Host "===========================================================================" -ForegroundColor Yellow
    Write-Host "WinPE Phase (setup.wim): Mount Setup WIM" -ForegroundColor Yellow
    Mount-WindowsImage -ImagePath $ImagePath -Index 1 -Path "$MountSetup" -Optimize -LogPath "$LogPath\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Mount-WindowsImage-setup.wim.log"

    Write-Host "===========================================================================" -ForegroundColor Yellow
    Write-Host "WinPE Phase (setup.wim): Servicing Update" -ForegroundColor Yellow

    if ($CustomServicingStack.IsPresent) {
        Write-Host "setup.wim:"
        Add-WindowsPackage -Path "$MountSetup" -PackagePath "$($UpdateCatServicing.FullName)" -LogPath "$LogPath\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-UpdateServicing-setup.wim.log"
    } else {
        foreach ($Update in $UpdateCatServicing) {
            $UpdateSSU = $(Get-ChildItem -Path $OSBuilderContent\Updates -Directory -Recurse | Where-Object {$_.Name -eq $($Update.KBTitle)}).FullName
            if (Test-Path "$UpdateSSU") {
                Write-Host "setup.wim: $UpdateSSU"
                if (Get-WindowsPackage -Path "$MountSetup" | Where-Object {$_.PackageName -like "*$($Update.KBNumber)*"}) {
                    Write-Warning "KB$($Update.KBNumber) Installed ... Skipping Update"
                } else {
                    Add-WindowsPackage -Path "$MountSetup" -PackagePath "$UpdateSSU" -LogPath "$LogPath\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-UpdateServicing-KB$($Update.KBNumber)-setup.wim.log"
                }
            } else {
                Write-Warning "Not Found: $UpdateSSU"
            }
        }
    }

    Write-Host "===========================================================================" -ForegroundColor Yellow
    Write-Host "WinPE Phase (setup.wim): Cumulative Update" -ForegroundColor Yellow
    if (!($null -eq $UpdateCatCumulative)) {
        if ($CustomCumulativeUpdate.IsPresent) {
            Write-Host "setup.wim:"
            Add-WindowsPackage -Path "$MountSetup" -PackagePath "$($UpdateCatCumulative.FullName)" -LogPath "$LogPath\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-UpdateCumulative-setup.wim.log"
            Dism /Image:"$MountSetup" /Cleanup-Image /StartComponentCleanup /ResetBase /LogPath:"$LogPath\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Dism-Cleanup-Image-setup.wim.log"
        } else {
            foreach ($Update in $UpdateCatCumulative) {
                $UpdateCU = $(Get-ChildItem -Path $OSBuilderContent\Updates -Directory -Recurse | Where-Object {$_.Name -eq $($Update.KBTitle)}).FullName
                if (Test-Path "$UpdateCU") {
                    Write-Host "setup.wim: $UpdateCU"
                    #if (Get-WindowsPackage -Path "$MountSetup" | Where-Object {$_.PackageName -like "*$($Update.KBNumber)*"}) {}
                    $SessionsXmlSetup = "$MountSetup\Windows\Servicing\Sessions\Sessions.xml"
                    if (Test-Path $SessionsXmlSetup) {
                        [xml]$XmlDocument = Get-Content -Path $SessionsXmlSetup
                        if ($XmlDocument.Sessions.Session.Tasks.Phase.package | Where-Object {$_.Name -like "*$($Update.KBNumber)*" -and $_.targetState -eq 'Installed'}) {
                            Write-Warning "KB$($Update.KBNumber) Installed ... Skipping Update"
                        } else {
                            Add-WindowsPackage -Path "$MountSetup" -PackagePath "$UpdateCU" -LogPath "$LogPath\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-UpdateCumulative-KB$($Update.KBNumber)-setup.wim.log"
                            Dism /Image:"$MountSetup" /Cleanup-Image /StartComponentCleanup /ResetBase /LogPath:"$LogPath\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Dism-Cleanup-Image-setup.wim.log"
                        }
                    } else {
                        Add-WindowsPackage -Path "$MountSetup" -PackagePath "$UpdateCU" -LogPath "$LogPath\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-UpdateCumulative-KB$($Update.KBNumber)-setup.wim.log"
                        Dism /Image:"$MountSetup" /Cleanup-Image /StartComponentCleanup /ResetBase /LogPath:"$LogPath\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Dism-Cleanup-Image-setup.wim.log"
                    }
                } else {
                    Write-Warning "Not Found: $UpdateCU"
                }
            }
        }
    }
    
    Write-Host "===========================================================================" -ForegroundColor Yellow
    Write-Host "WinPE Phase (setup.wim): Updating Media Sources with Setup.wim" -ForegroundColor Yellow

    #[void](Read-Host 'Press Enter to Continue')
    robocopy "$MountSetup\sources" "$OS\sources" setup.exe /e /ndl /xo /xx /xl /b /np /ts /tee /r:0 /w:0 /log:"$Logs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Robocopy-setup.wim-MediaSources.log"
    robocopy "$MountSetup\sources" "$OS\sources" setuphost.exe /e /ndl /xo /xx /xl /b /np /ts /tee /r:0 /w:0 /log:"$Logs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Robocopy-setup.wim-MediaSources.log"
    
    Write-Host "===========================================================================" -ForegroundColor Yellow
    Write-Host "WinPE Phase (setup.wim): Exporting Package Inventory" -ForegroundColor Yellow

    Write-Host "$PEInfo\setup-WindowsPackage.txt"
    Export-WindowsPackageInformation -MountPath $MountSetup -OutPath $PEInfo -Prefix "setup"

    Write-Host "===========================================================================" -ForegroundColor Yellow
    Write-Host "WinPE Phase (setup.wim): Dismount and Save" -ForegroundColor Yellow
    Write-Host "setup.wim: Dismount and Save $MountSetup"
    Dismount-WindowsImage -Path "$MountSetup" -Save -LogPath "$LogPath\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Dismount-WindowsImage-setup.wim.log" | Out-Null
   
    Write-Host "===========================================================================" -ForegroundColor Yellow
    Write-Host "WinPE Phase (setup.wim): Exporting WinPE WIMs" -ForegroundColor Yellow
    Write-Host "setup.wim: Exporting to $WinPE\setup.wim"
    Export-WindowsImage -SourceImagePath "$WimTemp\setup.wim" -SourceIndex 1 -DestinationImagePath "$WinPE\setup.wim" -LogPath "$LogPath\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Export-WindowsImage-setup.wim.log" | Out-Null

    if (Test-Path "$MountSetup") {Remove-Item -Path "$MountSetup" -Force -Recurse | Out-Null}
}