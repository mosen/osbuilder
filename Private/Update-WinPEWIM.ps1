function Update-WinPEWIM
{
    <#
    .DESCRIPTION
        This cmdlet contains only the Update-OSMedia steps regarding the winpe.wim file
    
    .PARAMETER LogPath
        Output path for logs. This was conventionally the PELogs path aka WinPE\info\logs
    .PARAMETER UpdateCatServicing
        The Servicing Stack Update Catalog, Downloaded from the OSBuilder.Public Repo.
    .PARAMETER UpdateCatCumulative
        The Cumulative Update Catalog, Downloaded from the OSBuilder.Public Repo.
    #>
    [CmdletBinding()]
    Param(
        [parameter(Mandatory=$true)]
        [string] $ImagePath,

        [parameter(Mandatory=$true)]
        [string] $OSBuilderContent,

        [parameter()]
        [string] $LogPath,

        [switch]$CustomCumulativeUpdate,
        [switch]$CustomServicingStack,

        $UpdateCatServicing,
        $UpdateCatCumulative
    )

    $MountWinPE = Join-Path $OSBuilderContent\Mount "winpe$((Get-Date).ToString('mmss'))"
    if ( ! (Test-Path "$MountWinPE")) {New-Item "$MountWinPE" -ItemType Directory -Force | Out-Null}

    Write-Host "===========================================================================" -ForegroundColor Yellow
    Write-Host "WinPE Phase (winpe.wim): Mount WinPE WIM" -ForegroundColor Yellow
    Mount-WindowsImage -ImagePath "$WimTemp\winpe.wim" -Index 1 -Path "$MountWinPE" -Optimize -LogPath "$PELogs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Mount-WindowsImage-winpe.wim.log"

    Write-Host "===========================================================================" -ForegroundColor Yellow
    Write-Host "WinPE Phase (winpe.wim): Servicing Update" -ForegroundColor Yellow

    if (!($null -eq $UpdateCatServicing)) {
        if ($CustomServicingStack.IsPresent) {
            Write-Host "winpe.wim:"
            Add-WindowsPackage -Path "$MountWinPE" -PackagePath "$($UpdateCatServicing.FullName)" -LogPath "$PELogs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-UpdateServicing-winpe.wim.log"
        } else {
            foreach ($Update in $UpdateCatServicing) {
                $UpdateSSU = $(Get-ChildItem -Path $OSBuilderContent\Updates -Directory -Recurse | Where-Object {$_.Name -eq $($Update.KBTitle)}).FullName
                if (Test-Path "$UpdateSSU") {
                    Write-Host "winpe.wim: $UpdateSSU"
                    if (Get-WindowsPackage -Path "$MountWinPE" | Where-Object {$_.PackageName -like "*$($Update.KBNumber)*"}) {
                        Write-Warning "KB$($Update.KBNumber) Installed ... Skipping Update"
                    } else {
                        Add-WindowsPackage -Path "$MountWinPE" -PackagePath "$UpdateSSU" -LogPath "$PELogs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-UpdateServicing-KB$($Update.KBNumber)-winpe.wim.log"
                    }
                } else {
                    Write-Warning "Not Found: $UpdateSSU"
                }
            }
        }
    }

    Write-Host "===========================================================================" -ForegroundColor Yellow
    Write-Host "WinPE Phase (winpe.wim): Cumulative Update" -ForegroundColor Yellow
    if (!($null -eq $UpdateCatCumulative)) {
        if ($CustomCumulativeUpdate.IsPresent) {
            Write-Host "winpe.wim:"
            Add-WindowsPackage -Path "$MountWinPE" -PackagePath "$($UpdateCatCumulative.FullName)" -LogPath "$PELogs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-UpdateCumulative-winpe.wim.log"
            Dism /Image:"$MountWinPE" /Cleanup-Image /StartComponentCleanup /ResetBase /LogPath:"$PELogs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Dism-Cleanup-Image-winpe.wim.log"
        } else {
            foreach ($Update in $UpdateCatCumulative) {
                $UpdateCU = $(Get-ChildItem -Path $OSBuilderContent\Updates -Directory -Recurse | Where-Object {$_.Name -eq $($Update.KBTitle)}).FullName
                if (Test-Path "$UpdateCU") {
                    Write-Host "winpe.wim: $UpdateCU"
                    #if (Get-WindowsPackage -Path "$MountWinPE" | Where-Object {$_.PackageName -like "*$($Update.KBNumber)*"}) {}
                    $SessionsXmlWinPE = "$MountWinPE\Windows\Servicing\Sessions\Sessions.xml"
                    if (Test-Path $SessionsXmlWinPE) {
                        [xml]$XmlDocument = Get-Content -Path $SessionsXmlWinPE
                        if ($XmlDocument.Sessions.Session.Tasks.Phase.package | Where-Object {$_.Name -like "*$($Update.KBNumber)*" -and $_.targetState -eq 'Installed'}) {
                            Write-Warning "KB$($Update.KBNumber) Installed ... Skipping Update"
                        } else {
                            Add-WindowsPackage -Path "$MountWinPE" -PackagePath "$UpdateCU" -LogPath "$PELogs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-UpdateCumulative-KB$($Update.KBNumber)-winpe.wim.log"
                            Dism /Image:"$MountWinPE" /Cleanup-Image /StartComponentCleanup /ResetBase /LogPath:"$PELogs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Dism-Cleanup-Image-winpe.wim.log"
                        }
                    } else {
                        Add-WindowsPackage -Path "$MountWinPE" -PackagePath "$UpdateCU" -LogPath "$PELogs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-UpdateCumulative-KB$($Update.KBNumber)-winpe.wim.log"
                        Dism /Image:"$MountWinPE" /Cleanup-Image /StartComponentCleanup /ResetBase /LogPath:"$PELogs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Dism-Cleanup-Image-winpe.wim.log"
                    }
                } else {
                    Write-Warning "Not Found: $UpdateCU"
                }
            }
        }
    }

    Write-Host "===========================================================================" -ForegroundColor Yellow
    Write-Host "WinPE Phase (winpe.wim): Exporting Package Inventory" -ForegroundColor Yellow

    Write-Host "$PEInfo\winpe-WindowsPackage.txt"
    Export-WindowsPackageInformation -MountPath $MountWinPE -OutPath $PEInfo -Prefix "winpe"

    Write-Host "===========================================================================" -ForegroundColor Yellow
    Write-Host "WinPE Phase (winpe.wim): Dismount and Save" -ForegroundColor Yellow
    Write-Host "winpe.wim: Dismount and Save $MountWinPE"
    Dismount-WindowsImage -Path "$MountWinPE" -Save -LogPath "$PELogs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Dismount-WindowsImage-winpe.wim.log" | Out-Null

    Write-Host "===========================================================================" -ForegroundColor Yellow
    Write-Host "WinPE Phase (winpe.wim): Exporting WinPE WIMs" -ForegroundColor Yellow
    Write-Host "winpe.wim: Exporting to $WinPE\winpe.wim"
    Export-WindowsImage -SourceImagePath "$WimTemp\winpe.wim" -SourceIndex 1 -DestinationImagePath "$WinPE\winpe.wim" -LogPath "$PELogs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Export-WindowsImage-winpe.wim.log" | Out-Null

    if (Test-Path "$MountWinPE") {Remove-Item -Path "$MountWinPE" -Force -Recurse | Out-Null}
}