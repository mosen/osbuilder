
function Import-WindowsMedia
{
	<#
	.SYNOPSIS
        Import a single Windows Installer Media into the OSBuilder working directory
    .DESCRIPTION
        Import a Windows Installer into an OSBuilder working directory.

        Each Windows Edition is copied into a different directory with a predictable naming scheme, for eg. Windows 10 Education 1709
            
            Win10 Edu x64 1709 en-GB 16299.15

        The name is comprised of `$ImageName $OSArchitecture $OSVersionNumber $OSLanguages $UBR`

    .PARAMETER ImagePath
        Full path to the installer image, usually sources\install.wim

    .PARAMETER ImageIndex
        The index of the current OS installer inside install.wim, varies depending on Windows edition.

    .PARAMETER Destination
        The destination path for the OS Media, expected to be the name returned by Format-WindowsInstallMediaName normally.
	#>
	[CmdletBinding()]
	Param (
		[parameter(Mandatory=$true)]
		[string] $ImagePath,

		[parameter(Mandatory=$true)]
		[int] $ImageIndex,

		[parameter()]
        [string] $Destination
    )

    $Info = Join-Path $Destination 'info'
    $Logs =	Join-Path $Info "logs"
    
    Write-Host "Destination Directory: $($Destination)"
    
    $OS = Join-Path $WorkingPath "OS"
    $WinPE = Join-Path $WorkingPath "WinPE"
    if (!(Test-Path "$OS"))			{New-Item "$OS" -ItemType Directory -Force | Out-Null}
    if (!(Test-Path "$WinPE"))		{New-Item "$WinPE" -ItemType Directory -Force | Out-Null}

    Write-Host "===========================================================================" -ForegroundColor Yellow
    Write-Host "Importing Operating System to OSMedia" -ForegroundColor Yellow
    Write-Host "Copying OS to $OS"

    $OSSourcePath = Split-Path -Path "$ImagePath" -Qualifier
    Write-Host "Copying from: $OSSourcePath"

    Copy-Item -Path "$OSSourcePath\*" -Destination "$OS" -Exclude install.wim -Recurse -Force | Out-Null
    Write-Host "Removing the Read Only file flag in $OS"
    Get-ChildItem -Recurse -Path "$OS\*" | Set-ItemProperty -Name IsReadOnly -Value $false -ErrorAction SilentlyContinue | Out-Null

    Write-Host "Exporting Index $ImageIndex to $OS\sources\install.wim"
    Export-WindowsImage -SourceImagePath "$ImagePath" -SourceIndex $ImageIndex -DestinationImagePath "$OS\sources\install.wim" -LogPath "$Logs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Export-WindowsImage.log"
    

    Write-Host "===========================================================================" -ForegroundColor Yellow
    Write-Host "Exporting WinPE WIMs" -ForegroundColor Yellow
    Write-Host "$WinPE\boot.wim"
    Copy-Item -Path "$OS\sources\boot.wim" -Destination "$WinPE\boot.wim" -Force

    Write-Host "$WinPE\winpe.wim"
    Export-WindowsImage -SourceImagePath "$OS\sources\boot.wim" -SourceIndex 1 -DestinationImagePath "$WinPE\winpe.wim" -LogPath "$PELogs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Export-WindowsImage-winpe.wim.log"

    Write-Host "$WinPE\setup.wim"
    Export-WindowsImage -SourceImagePath "$OS\sources\boot.wim" -SourceIndex 2 -DestinationImagePath "$WinPE\setup.wim" -LogPath "$PELogs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Export-WindowsImage-setup.wim.log"

}
