function Test-AdminRights {
    #======================================================================================
    #   Validate Administrator Rights 18.9.27
    #======================================================================================
    if (!([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) {
        Throw "OSBuilder: This function needs to be run as Administrator"
    }
}