function Export-WindowsPackageInformation
{
    <#
    .SYNOPSIS
        Export information about Windows Packages from a mounted .wim image.

    .PARAMETER MountPath
        This is the path where the .wim has been mounted.
    .PARAMETER Prefix
        This prefix will be appended to any text/xml/json generated, usually this is the same as the wim basename.
    .PARAMETER OutPath
        This is the root of the directory where information will be written. By convention this is to be `WinPE`
    #>
    [CmdletBinding()]
    Param(
        [parameter(Mandatory=$true)]
        [string] $MountPath,

        [parameter(Mandatory=$true)]
        [string] $Prefix,

        [parameter(Mandatory=$true)]
        [string] $OutPath
    )

    $PEInfo = Join-Path $OutPath 'info'
    $PELogsJS = Join-Path $PEInfo 'json'
    $PELogsXML = Join-Path $PEInfo 'xml'
    $PELogs =	Join-Path $PEInfo "logs"

    if (!(Test-Path "$PEInfo"))		{New-Item "$PEInfo" -ItemType Directory -Force | Out-Null}
    if (!(Test-Path "$PELogsJS"))	{New-Item "$PELogsJS" -ItemType Directory -Force | Out-Null}
    if (!(Test-Path "$PELogsXML"))	{New-Item "$PELogsXML" -ItemType Directory -Force | Out-Null}
    if (!(Test-Path "$PELogs"))		{New-Item "$PELogs" -ItemType Directory -Force | Out-Null}

    Write-Host "$OutPath\$Prefix-WindowsPackage.txt"
    $GetWindowsPackage = Get-WindowsPackage -Path "$MountPath"
    $GetWindowsPackage | Out-File "$PEInfo\$Prefix-WindowsPackage.txt"
    $GetWindowsPackage | Out-File "$PELogs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Get-WindowsPackage-$Prefix.wim.txt"
    $GetWindowsPackage | Export-Clixml -Path "$PELogsXML\Get-WindowsPackage-$Prefix.wim.xml"
    $GetWindowsPackage | Export-Clixml -Path "$PELogsXML\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Get-WindowsPackage-$Prefix.wim.xml"
    $GetWindowsPackage | ConvertTo-Json | Out-File "$PELogsJS\Get-WindowsPackage-$Prefix.wim.json"
    $GetWindowsPackage | ConvertTo-Json | Out-File "$PELogsJS\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Get-WindowsPackage-$Prefix.wim.json"
}