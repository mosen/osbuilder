function Update-InstallWIM
{
    [CmdletBinding()]
    Param(
        $ImagePath
    )

    #======================================================================================
    #   Mount Install.wim 18.9.10
    #======================================================================================
    Write-Host "===========================================================================" -ForegroundColor Yellow
    Write-Host "Install.wim Phase: Mounting Operating System" -ForegroundColor Yellow
    Write-Host $MountDirectory
    Mount-WindowsImage -ImagePath "$WimTemp\install.wim" -Index 1 -Path "$MountDirectory" -Optimize -LogPath "$Logs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Mount-WindowsImage.log"


    #======================================================================================
    #   Get Registry and UBR 18.9.20
    #======================================================================================
    reg LOAD 'HKLM\OSMedia' "$MountDirectory\Windows\System32\Config\SOFTWARE"
    $RegCurrentVersion = Get-ItemProperty -Path 'HKLM:\OSMedia\Microsoft\Windows NT\CurrentVersion'
    reg UNLOAD 'HKLM\OSMedia'

    $OSVersionNumber = $null
    $OSVersionNumber = $($RegCurrentVersion.ReleaseId)
    $RegCurrentVersionUBR = $($RegCurrentVersion.UBR)
    $UBR = "$OSBuild.$RegCurrentVersionUBR"
    #======================================================================================
    #   Export RegCurrentVersion 18.9.20
    #======================================================================================
    $RegCurrentVersion | Out-File "$Info\CurrentVersion.txt"
    $RegCurrentVersion | Out-File "$WorkingPath\CurrentVersion.txt"
    $RegCurrentVersion | Out-File "$Logs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-CurrentVersion.txt"
    $RegCurrentVersion | Export-Clixml -Path "$LogsXML\CurrentVersion.xml"
    $RegCurrentVersion | Export-Clixml -Path "$LogsXML\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-CurrentVersion.xml"
    $RegCurrentVersion | ConvertTo-Json | Out-File "$LogsJS\CurrentVersion.json"
    $RegCurrentVersion | ConvertTo-Json | Out-File "$LogsJS\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-CurrentVersion.json"
    #======================================================================================
    #   Replace WinRE 18.9.10
    #======================================================================================
    Write-Host "===========================================================================" -ForegroundColor Yellow
    Write-Host "Install.wim Phase: Replacing WinRE.wim" -ForegroundColor Yellow
    Write-Host "Removing existing $MountDirectory\Windows\System32\Recovery\winre.wim"
    if (Test-Path "$MountDirectory\Windows\System32\Recovery\winre.wim") {
        Remove-Item -Path "$MountDirectory\Windows\System32\Recovery\winre.wim" -Force
    }
    #======================================================================================
    Write-Host "Copying WinRE.wim to $MountDirectory\Windows\System32\Recovery\winre.wim"
    Copy-Item -Path "$WinPE\winre.wim" -Destination "$MountDirectory\Windows\System32\Recovery\winre.wim" -Force | Out-Null
    #======================================================================================
    Write-Host "Generating WinRE.wim info"
    Export-WinREImageInformation -ImagePath "$WinPE\winre.wim" -OutPath "$WorkingPath"
    #======================================================================================
    #   Install.wim Servicing Update 18.10.3
    #======================================================================================
    Write-Host "===========================================================================" -ForegroundColor Yellow
    Write-Host "Install.wim Phase: Servicing Update" -ForegroundColor Yellow
    if (!($null -eq $UpdateCatServicing)) {
        if ($CustomServicingStack.IsPresent) {
            Write-Host "install.wim:"
            Add-WindowsPackage -Path "$MountDirectory" -PackagePath "$($UpdateCatServicing.FullName)" -LogPath "$Logs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-UpdateServicing-install.wim.log"
        } else {
            foreach ($Update in $UpdateCatServicing) {
                $UpdateSSU = $(Get-ChildItem -Path $OSBuilderContent\Updates -Directory -Recurse | Where-Object {$_.Name -eq $($Update.KBTitle)}).FullName
                if (Test-Path "$UpdateSSU") {
                    Write-Host "install.wim: $UpdateSSU"
                    if (Get-WindowsPackage -Path "$MountDirectory" | Where-Object {$_.PackageName -like "*$($Update.KBNumber)*"}) {
                        Write-Warning "KB$($Update.KBNumber) Installed ... Skipping Update"
                    } else {
                        Add-WindowsPackage -Path "$MountDirectory" -PackagePath "$UpdateSSU" -LogPath "$Logs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-UpdateServicing-KB$($Update.KBNumber)-install.wim.log"
                    }
                } else {
                    Write-Warning "Not Found: $UpdateSSU"
                }
            }
        }
    }
    #======================================================================================
    #   Install.wim Component Update 18.10.3
    #======================================================================================
    Write-Host "===========================================================================" -ForegroundColor Yellow
    Write-Host "Install.wim Phase: Component Update" -ForegroundColor Yellow
    if (!($null -eq $UpdateCatComponent)) {
        foreach ($Update in $UpdateCatComponent) {
            $UpdateComp = $(Get-ChildItem -Path $OSBuilderContent\Updates -Directory -Recurse | Where-Object {$_.Name -eq $($Update.KBTitle)}).FullName
            if (Test-Path "$UpdateComp") {
                Write-Host "install.wim: $UpdateComp"
                if (Get-WindowsPackage -Path "$MountDirectory" | Where-Object {$_.PackageName -like "*$($Update.KBNumber)*"}) {
                    Write-Warning "KB$($Update.KBNumber) Installed ... Skipping Update"
                } else {
                    Add-WindowsPackage -Path "$MountDirectory" -PackagePath "$UpdateComp" -LogPath "$PELogs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-UpdateComponent-KB$($Update.KBNumber)-install.wim.log"
                }
            } else {
                Write-Warning "Not Found: $UpdateComp"
            }
        }
    }
    #======================================================================================
    #   Get UBR (Pre Windows Updates) 18.9.20
    #======================================================================================
    $UBRPre = $UBR
    Write-Host "===========================================================================" -ForegroundColor Yellow
    Write-Host "Install.wim Phase: Update Build Revision $UBRPre (Pre-Windows Updates)"	-ForegroundColor Yellow
    #======================================================================================
    #   Install.wim Cumulative Update 18.10.3
    #======================================================================================
    Write-Host "===========================================================================" -ForegroundColor Yellow
    Write-Host "Install.wim Phase: Cumulative Update" -ForegroundColor Yellow
    if (!($null -eq $UpdateCatCumulative)) {
        if ($CustomCumulativeUpdate.IsPresent) {
            Add-WindowsPackage -Path "$MountDirectory" -PackagePath "$($UpdateCatCumulative.FullName)" -LogPath "$Logs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-UpdateCumulative-install.wim.log"
        } else {
            foreach ($Update in $UpdateCatCumulative) {
                $UpdateCU = $(Get-ChildItem -Path $OSBuilderContent\Updates -Directory -Recurse | Where-Object {$_.Name -eq $($Update.KBTitle)}).FullName
                if (Test-Path "$UpdateCU") {
                    Write-Host "install.wim: $UpdateCU"
                    $SessionsXmlInstall = "$MountDirectory\Windows\Servicing\Sessions\Sessions.xml"
                    if (Test-Path $SessionsXmlInstall) {
                        [xml]$XmlDocument = Get-Content -Path $SessionsXmlInstall
                        if ($XmlDocument.Sessions.Session.Tasks.Phase.package | Where-Object {$_.Name -like "*$($Update.KBNumber)*" -and $_.targetState -eq 'Installed'}) {
                            Write-Warning "KB$($Update.KBNumber) Installed ... Skipping Update"
                        } else {
                            Add-WindowsPackage -Path "$MountDirectory" -PackagePath "$UpdateCU" -LogPath "$Logs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-UpdateCumulative-KB$($Update.KBNumber)-install.wim.log"
                        }
                    } else {
                        Add-WindowsPackage -Path "$MountDirectory" -PackagePath "$UpdateCU" -LogPath "$Logs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-UpdateCumulative-KB$($Update.KBNumber)-install.wim.log"
                    }
                } else {
                    Write-Warning "Not Found: $UpdateCU"
                }
            }
        }
    }
    #======================================================================================
    #   Get Registry and UBR 18.9.20
    #======================================================================================
    reg LOAD 'HKLM\OSMedia' "$MountDirectory\Windows\System32\Config\SOFTWARE"
    $RegCurrentVersion = Get-ItemProperty -Path 'HKLM:\OSMedia\Microsoft\Windows NT\CurrentVersion'
    reg UNLOAD 'HKLM\OSMedia'

    $OSVersionNumber = $null
    $OSVersionNumber = $($RegCurrentVersion.ReleaseId)
    $RegCurrentVersionUBR = $($RegCurrentVersion.UBR)
    $UBR = "$OSBuild.$RegCurrentVersionUBR"

    #======================================================================================
    #   Get UBR (Post Windows Updates) 18.9.20
    #======================================================================================
    Write-Host "===========================================================================" -ForegroundColor Yellow
    Write-Host "Install.wim Phase: Update Build Revision $UBR (Post-Windows Updates)"	-ForegroundColor Yellow
    #======================================================================================
    #   Install.wim Adobe Update 18.9.24
    #======================================================================================
    Write-Host "===========================================================================" -ForegroundColor Yellow
    Write-Host "Install.wim Phase: Adobe Update" -ForegroundColor Yellow
    if (!($null -eq $UpdateCatAdobe)) {
        foreach ($Update in $UpdateCatAdobe) {
            $UpdateA = $(Get-ChildItem -Path $OSBuilderContent\Updates -Directory -Recurse | Where-Object {$_.Name -eq $($Update.KBTitle)}).FullName
            if (Test-Path "$UpdateA") {
                Write-Host "install.wim: $UpdateA"
                if (Get-WindowsPackage -Path "$MountDirectory" | Where-Object {$_.PackageName -like "*$($Update.KBNumber)*"}) {
                    Write-Warning "KB$($Update.KBNumber) Installed ... Skipping Update"
                } else {
                    Add-WindowsPackage -Path "$MountDirectory" -PackagePath "$UpdateA" -LogPath "$Logs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-UpdateAdobe-KB$($Update.KBNumber)-install.wim.log"
                }
            } else {
                Write-Warning "Not Found: $UpdateA"
            }
        }
    }
    #======================================================================================
    #   Install.wim Image Cleanup 18.9.10
    #======================================================================================
    Write-Host "===========================================================================" -ForegroundColor Yellow
    Write-Host "Install.wim Phase: Windows Image Cleanup" -ForegroundColor Yellow
    if ($(Get-WindowsCapability -Path $MountDirectory | Where-Object {$_.state -eq "*pending*"})) {
        Write-Warning "Cannot run WindowsImage Cleanup on a WIM with Pending Installations"
    } else {
        Write-Host "Performing Image Cleanup on $MountDirectory"
        Dism /Image:"$MountDirectory" /Cleanup-Image /StartComponentCleanup /ResetBase /LogPath:"$Logs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Dism-Cleanup-Image.log"
    }    

    #======================================================================================
    #   Install.wim Save Mounted Windows Image Configuration 18.9.10
    #======================================================================================
    Write-Host "===========================================================================" -ForegroundColor Yellow
    Write-Host "Install.wim Phase: Saving Mounted Windows Image Configuration" -ForegroundColor Yellow
    #======================================================================================
    Export-WindowsMediaInformation -MountDirectory "$MountDirectory" -OutPath "$WorkingPath"

    #======================================================================================
    #   Dismount and Save 18.10.2
    #======================================================================================
    Write-Host "===========================================================================" -ForegroundColor Yellow
    Write-Host "Install.wim Phase: Dismount and Save" -ForegroundColor Yellow
    Write-Host "Dismount and Save $MountDirectory"
    Dismount-WindowsImage -Path "$MountDirectory" -Save -LogPath "$Logs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Dismount-WindowsImage.log"
    #======================================================================================
    #   Export Install.wim 18.10.2
    #======================================================================================
    Write-Host "===========================================================================" -ForegroundColor Yellow
    Write-Host "Install.wim Phase: Export Install.wim" -ForegroundColor Yellow
    Write-Host "Exporting $WimTemp\install.wim"
    Export-WindowsImage -SourceImagePath "$WimTemp\install.wim" -SourceIndex 1 -DestinationImagePath "$OS\sources\install.wim" -LogPath "$Logs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Export-WindowsImage.log"
    
    #======================================================================================
    #   Saving Windows Image Configuration 18.10.2
    #======================================================================================
    Write-Host "===========================================================================" -ForegroundColor Yellow
    Write-Host "Inventory Phase: Saving Windows Image Configuration" -ForegroundColor Yellow
    Write-Host "$WorkingPath\WindowsImage.txt"

    $GetWindowsImage = Get-WindowsImage -ImagePath "$OS\sources\install.wim" -Index 1
    Export-WindowsImageInformation -WindowsImage $GetWindowsImage -OutPath "$WorkingPath" -UBR $UBR
    
}