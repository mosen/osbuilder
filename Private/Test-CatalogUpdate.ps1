function Test-CatalogUpdate {
    <#
    .SYNOPSIS
        Test that an OSBuilder Catalog Update matches the specified criteria.
    .DESCRIPTION
        To be used a as a predicate for Where-Object
    #>
    Param(
        [parameter(Mandatory=$true)]
        $CatalogUpdate,

        $Category = "",
        $OSVersionNumber = "",
        $OSArchitecture = "",
        $OSInstallationType = ""
    )

    if (!($CatalogUpdate.Category -eq $Category)) { return $false }
    if (!($CatalogUpdate.KBTitle -like "*$OSVersionNumber*")) { return $false }
    if (!($CatalogUpdate.KBTitle -like "*$OSArchitecture*")) { return $false }

    if ($OSInstallationType -like "*Server*") {
       if (!($CatalogUpdate.KBTitle -like "*Server*")) { return $false }
    } else {
       if ($CatalogUpdate.KBTitle -like "*Server*") { return $false }
    }

    return $true
}