function Export-BootImageInformation
{
    <#
    .SYNOPSIS
        Export information about a windows boot image (boot.wim)

    .DESCRIPTION
        Export metadata about images contained within the boot image:

        - The boot image itself
        - Win PE environment
        - Setup environment

    .PARAMETER ImagePath
        Full path to boot.wim

    .PARAMETER OutPath
        The directory where information about the image will be saved in multiple formats.

    #>
    [CmdletBinding()]
    Param(
        [parameter(Mandatory=$true, ValueFromPipeline=$true)]
        $ImagePath,

        [parameter(Mandatory=$true)]
        $OutPath
    )

    Write-Host "===========================================================================" -ForegroundColor Yellow
    Write-Host "Saving WinPE Image Configuration" -ForegroundColor Yellow

    $WinPE = Join-Path $OutPath "WinPE"
    if (!(Test-Path "$WinPE"))		{New-Item "$WinPE" -ItemType Directory -Force | Out-Null}

    $PEInfo = Join-Path $WinPE 'info'
    $PELogsJS = Join-Path $PEInfo 'json'
    $PELogsXML = Join-Path $PEInfo 'xml'
    $PELogs =	Join-Path $PEInfo "logs"
    if (!(Test-Path "$PEInfo"))		{New-Item "$PEInfo" -ItemType Directory -Force | Out-Null}
    if (!(Test-Path "$PELogsJS"))	{New-Item "$PELogsJS" -ItemType Directory -Force | Out-Null}
    if (!(Test-Path "$PELogsXML"))	{New-Item "$PELogsXML" -ItemType Directory -Force | Out-Null}
    if (!(Test-Path "$PELogs"))		{New-Item "$PELogs" -ItemType Directory -Force | Out-Null}

    Write-Host "$PEInfo\boot.txt"
    $GetWindowsImage = Get-WindowsImage -ImagePath "$ImagePath"
    $GetWindowsImage | Out-File "$PEInfo\boot.txt"
    (Get-Content "$PEInfo\boot.txt") | Where-Object {$_.Trim(" `t")} | Set-Content "$PEInfo\boot.txt"
    $GetWindowsImage | Out-File "$PELogs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Get-WindowsImage-boot.wim.txt"
    $GetWindowsImage | Export-Clixml -Path "$PELogsXML\Get-WindowsImage-boot.wim.xml"
    $GetWindowsImage | Export-Clixml -Path "$PELogsXML\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Get-WindowsImage-boot.wim.xml"
    $GetWindowsImage | ConvertTo-Json | Out-File "$PELogsJS\Get-WindowsImage-boot.wim.json"
    $GetWindowsImage | ConvertTo-Json | Out-File "$PELogsJS\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Get-WindowsImage-boot.wim.json"

    Write-Host "$PEInfo\winpe.txt"
    $GetWindowsImage = Get-WindowsImage -ImagePath "$ImagePath" -Index 1 | Select-Object -Property *
    $GetWindowsImage | Out-File "$PEInfo\winpe.txt"
    (Get-Content "$PEInfo\winpe.txt") | Where-Object {$_.Trim(" `t")} | Set-Content "$PEInfo\winpe.txt"
    $GetWindowsImage | Out-File "$PELogs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Get-WindowsImage-winpe.wim.txt"
    $GetWindowsImage | Export-Clixml -Path "$PELogsXML\Get-WindowsImage-winpe.wim.xml"
    $GetWindowsImage | Export-Clixml -Path "$PELogsXML\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Get-WindowsImage-winpe.wim.xml"
    $GetWindowsImage | ConvertTo-Json | Out-File "$PELogsJS\Get-WindowsImage-winpe.wim.json"
    $GetWindowsImage | ConvertTo-Json | Out-File "$PELogsJS\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Get-WindowsImage-winpe.wim.json"

    Write-Host "$PEInfo\setup.txt"
    $GetWindowsImage = Get-WindowsImage -ImagePath "$ImagePath" -Index 2 | Select-Object -Property *
    $GetWindowsImage | Out-File "$PEInfo\setup.txt"
    (Get-Content "$PEInfo\setup.txt") | Where-Object {$_.Trim(" `t")} | Set-Content "$PEInfo\setup.txt"
    $GetWindowsImage | Out-File "$PELogs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Get-WindowsImage-setup.wim.txt"
    $GetWindowsImage | Export-Clixml -Path "$PELogsXML\Get-WindowsImage-setup.wim.xml"
    $GetWindowsImage | Export-Clixml -Path "$PELogsXML\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Get-WindowsImage-setup.wim.xml"
    $GetWindowsImage | ConvertTo-Json | Out-File "$PELogsJS\Get-WindowsImage-setup.wim.json"
    $GetWindowsImage | ConvertTo-Json | Out-File "$PELogsJS\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Get-WindowsImage-setup.wim.json"
}