function Export-WindowsImageInformation
{
    <#
    .SYNOPSIS
        Export information about an image within an install.wim

    .PARAMETER WindowsImage
        Windows image information previously extracted using `Get-WindowsImage -ImagePath -Index`
    .PARAMETER OutPath
        Output directory for information about the install image. Usually the OS\<OS Descriptive Name> path.
    .PARAMETER UBR
        Build Revision of the installer media which will be appended to the image information.
    #>
    [CmdletBinding()]
    Param(
        [parameter(Mandatory=$true, ValueFromPipeline=$true)]
        [Microsoft.Dism.Commands.ImageInfoObject] $WindowsImage,

        [parameter(Mandatory=$true)]
        [string] $OutPath,

        [parameter()]
        [string] $UBR

    )

    $Info = Join-Path $OutPath 'info'
    $LogsJS = Join-Path $Info 'json'
    $LogsXML = Join-Path $Info 'xml'
    $Logs =	Join-Path $Info "logs"
    if (!(Test-Path "$Info"))		{New-Item "$Info" -ItemType Directory -Force | Out-Null}
    if (!(Test-Path "$LogsJS"))		{New-Item "$LogsJS" -ItemType Directory -Force | Out-Null}
    if (!(Test-Path "$LogsXML"))	{New-Item "$LogsXML" -ItemType Directory -Force | Out-Null}
    if (!(Test-Path "$Logs"))		{New-Item "$Logs" -ItemType Directory -Force | Out-Null}

    Write-Host "===========================================================================" -ForegroundColor Yellow
    Write-Host "Saving Windows Image Configuration" -ForegroundColor Yellow

    Write-Host "$OutPath\WindowsImage.txt"
    $GetWindowsImage = $WindowsImage | Select-Object -Property *
    $GetWindowsImage | Add-Member -Type NoteProperty -Name "UBR" -Value $UBR
    $GetWindowsImage | Out-File "$OutPath\WindowsImage.txt"
    $GetWindowsImage | Out-File "$Logs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Get-WindowsImage.txt"
    $GetWindowsImage | Export-Clixml -Path "$LogsXML\Get-WindowsImage.xml"
    $GetWindowsImage | Export-Clixml -Path "$LogsXML\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Get-WindowsImage.xml"
    $GetWindowsImage | ConvertTo-Json | Out-File "$LogsJS\Get-WindowsImage.json"
    $GetWindowsImage | ConvertTo-Json | Out-File "$LogsJS\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Get-WindowsImage.json"
    (Get-Content "$OutPath\WindowsImage.txt") | Where-Object {$_.Trim(" `t")} | Set-Content "$OutPath\WindowsImage.txt"

    Write-Host "$Info\Get-WindowsImageContent.txt"
    #Get-WindowsImageContent -ImagePath "$($WindowsImage.ImagePath)" -Index 1 | Out-File "$Info\Get-WindowsImageContent.txt"
}