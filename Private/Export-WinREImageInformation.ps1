function Export-WinREImageInformation 
{
    <#
    .SYNOPSIS
        Export information about a Windows Recovery (WinRE) image.

    .DESCRIPTION
        Export metadata about the Windows Recovery (WinRE) image.

    .PARAMETER ImagePath
        Full path to winre.wim

    .PARAMETER OutPath
        The directory where information about the image will be saved in multiple formats.
    #>
    [CmdletBinding()]
    Param(
        [parameter(Mandatory=$true, ValueFromPipeline=$true)]
        $ImagePath,

        [parameter(Mandatory=$true)]
        $OutPath
	)
	
	$WinPE = Join-Path $OutPath "WinPE"
    if (!(Test-Path "$WinPE"))		{New-Item "$WinPE" -ItemType Directory -Force | Out-Null}

    $PEInfo = Join-Path $WinPE 'info'
    $PELogsJS = Join-Path $PEInfo 'json'
    $PELogsXML = Join-Path $PEInfo 'xml'
    $PELogs =	Join-Path $PEInfo "logs"
    if (!(Test-Path "$PEInfo"))		{New-Item "$PEInfo" -ItemType Directory -Force | Out-Null}
    if (!(Test-Path "$PELogsJS"))	{New-Item "$PELogsJS" -ItemType Directory -Force | Out-Null}
    if (!(Test-Path "$PELogsXML"))	{New-Item "$PELogsXML" -ItemType Directory -Force | Out-Null}
    if (!(Test-Path "$PELogs"))		{New-Item "$PELogs" -ItemType Directory -Force | Out-Null}

	#======================================================================================
	#	Get-WindowsImage WinRE 18.9.13
	#======================================================================================
	Write-Host "$PEInfo\winre.txt"
	$GetWindowsImage = Get-WindowsImage -ImagePath "$ImagePath" -Index 1 | Select-Object -Property *
	$GetWindowsImage | Out-File "$PEInfo\winre.txt"
	(Get-Content "$PEInfo\winre.txt") | Where-Object {$_.Trim(" `t")} | Set-Content "$PEInfo\winre.txt"
	$GetWindowsImage | Out-File "$PELogs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Get-WindowsImage-winre.wim.txt"
	$GetWindowsImage | Export-Clixml -Path "$PELogsXML\Get-WindowsImage-winre.wim.xml"
	$GetWindowsImage | Export-Clixml -Path "$PELogsXML\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Get-WindowsImage-winre.wim.xml"
	$GetWindowsImage | ConvertTo-Json | Out-File "$PELogsJS\Get-WindowsImage-winre.wim.json"
	$GetWindowsImage | ConvertTo-Json | Out-File "$PELogsJS\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Get-WindowsImage-winre.wim.json"
}