function Import-WinREMedia
{
    [CmdletBinding()]
    Param(
        [parameter(Mandatory=$true, ValueFromPipeline=$true)]
        $MountDirectory,

        [parameter(Mandatory=$true)]
        $OutPath
    )
    
    $WinPE = Join-Path $OutPath "WinPE"
    if (!(Test-Path "$WinPE"))		{New-Item "$WinPE" -ItemType Directory -Force | Out-Null}

    $PEInfo = Join-Path $WinPE 'info'
    $PELogs =	Join-Path $PEInfo "logs"
    if (!(Test-Path "$PEInfo"))		{New-Item "$PEInfo" -ItemType Directory -Force | Out-Null}
    if (!(Test-Path "$PELogs"))		{New-Item "$PELogs" -ItemType Directory -Force | Out-Null}

    Write-Host "$WinPE\winre.wim"
    $ExportParams = @{
        SourceImagePath = "$MountDirectory\Windows\System32\Recovery\winre.wim";
        SourceIndex = 1;
        DestinationImagePath = "$WinPE\winre.wim";
        LogPath = "$PELogs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Export-WindowsImage-winre.wim.log"
    }
    Export-WindowsImage @ExportParams
}