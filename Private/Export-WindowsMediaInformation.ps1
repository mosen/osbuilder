function Export-WindowsMediaInformation
{
    <#
    .SYNOPSIS
        Export information about a mounted windows installation media

    .DESCRIPTION
        Export information about a mounted windows installation media, including the following details:

        - Offline registry information including the OS Version and Build Revision
        - Appx Provisioned Packages (If not a server image)
        - Windows Optional Features Available
        - Windows Capabilities
        - Windows Packages

        Information is exported to the OutPath, and subdirectories are created underneath for each type of format supported.

    .PARAMETER MountDirectory
        Path to a mounted windows image

    .PARAMETER OutPath
        The directory where information about the image will be saved in multiple formats.

    #>
    [CmdletBinding()]
    Param(
        [parameter(Mandatory=$true, ValueFromPipeline=$true)]
        $MountDirectory,

        [parameter(Mandatory=$true)]
        $OutPath
    )

    $Info = Join-Path $OutPath 'info'
    $LogsJS = Join-Path $Info 'json'
    $LogsXML = Join-Path $Info 'xml'
    $Logs =	Join-Path $Info "logs"
    if (!(Test-Path "$Info"))		{New-Item "$Info" -ItemType Directory -Force | Out-Null}
    if (!(Test-Path "$LogsJS"))		{New-Item "$LogsJS" -ItemType Directory -Force | Out-Null}
    if (!(Test-Path "$LogsXML"))	{New-Item "$LogsXML" -ItemType Directory -Force | Out-Null}
    if (!(Test-Path "$Logs"))		{New-Item "$Logs" -ItemType Directory -Force | Out-Null}

    Write-Host "===========================================================================" -ForegroundColor Yellow
    Write-Host "Saving Mounted Windows Image Configuration" -ForegroundColor Yellow

	Write-Verbose -Message "Loading Registry offline to retrieve OS Version and UBR"
	reg LOAD 'HKLM\OSMedia' "$MountDirectory\Windows\System32\Config\SOFTWARE"
	$RegCurrentVersion = Get-ItemProperty -Path 'HKLM:\OSMedia\Microsoft\Windows NT\CurrentVersion'
	reg UNLOAD 'HKLM\OSMedia'

    Write-Host "$Info\CurrentVersion.txt"
    $RegCurrentVersion | Out-File "$Info\CurrentVersion.txt"
    $RegCurrentVersion | Out-File "$WorkingPath\CurrentVersion.txt"
    $RegCurrentVersion | Out-File "$Logs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-CurrentVersion.txt"
    $RegCurrentVersion | Export-Clixml -Path "$LogsXML\CurrentVersion.xml"
    $RegCurrentVersion | Export-Clixml -Path "$LogsXML\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-CurrentVersion.xml"
    $RegCurrentVersion | ConvertTo-Json | Out-File "$LogsJS\CurrentVersion.json"
    $RegCurrentVersion | ConvertTo-Json | Out-File "$LogsJS\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-CurrentVersion.json"

    if ($ImageName -notlike "*server*") {
        $GetAppxProvisionedPackage = Get-AppxProvisionedPackage -Path "$MountDirectory"
        Write-Host "$OutPath\AppxProvisionedPackage.txt"
        $GetAppxProvisionedPackage | Out-File "$Info\Get-AppxProvisionedPackage.txt"
        $GetAppxProvisionedPackage | Out-File "$OutPath\AppxProvisionedPackage.txt"
        $GetAppxProvisionedPackage | Out-File "$Logs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Get-AppxProvisionedPackage.txt"
        $GetAppxProvisionedPackage | Export-Clixml -Path "$LogsXML\Get-AppxProvisionedPackage.xml"
        $GetAppxProvisionedPackage | Export-Clixml -Path "$LogsXML\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Get-AppxProvisionedPackage.xml"
        $GetAppxProvisionedPackage | ConvertTo-Json | Out-File "$LogsJS\Get-AppxProvisionedPackage.json"
        $GetAppxProvisionedPackage | ConvertTo-Json | Out-File "$LogsJS\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Get-AppxProvisionedPackage.json"
    }

    Write-Host "$OutPath\WindowsOptionalFeature.txt"
    $GetWindowsOptionalFeature = Get-WindowsOptionalFeature -Path "$MountDirectory"
    $GetWindowsOptionalFeature | Out-File "$Info\Get-WindowsOptionalFeature.txt"
    $GetWindowsOptionalFeature | Out-File "$OutPath\WindowsOptionalFeature.txt"
    $GetWindowsOptionalFeature | Out-File "$Logs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Get-WindowsOptionalFeature.txt"
    $GetWindowsOptionalFeature | Export-Clixml -Path "$LogsXML\Get-WindowsOptionalFeature.xml"
    $GetWindowsOptionalFeature | Export-Clixml -Path "$LogsXML\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Get-WindowsOptionalFeature.xml"
    $GetWindowsOptionalFeature | ConvertTo-Json | Out-File "$LogsJS\Get-WindowsOptionalFeature.json"
    $GetWindowsOptionalFeature | ConvertTo-Json | Out-File "$LogsJS\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Get-WindowsOptionalFeature.json"

    Write-Host "$OutPath\WindowsCapability.txt"
    $GetWindowsCapability = Get-WindowsCapability -Path "$MountDirectory"
    $GetWindowsCapability | Out-File "$Info\Get-WindowsCapability.txt"
    $GetWindowsCapability | Out-File "$OutPath\WindowsCapability.txt"
    $GetWindowsCapability | Out-File "$Logs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Get-WindowsCapability.txt"
    $GetWindowsCapability | Export-Clixml -Path "$LogsXML\Get-WindowsCapability.xml"
    $GetWindowsCapability | Export-Clixml -Path "$LogsXML\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Get-WindowsCapability.xml"
    $GetWindowsCapability | ConvertTo-Json | Out-File "$LogsJS\Get-WindowsCapability.json"
    $GetWindowsCapability | ConvertTo-Json | Out-File "$LogsJS\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Get-WindowsCapability.json"

    Write-Host "$OutPath\WindowsPackage.txt"
    $GetWindowsPackage = Get-WindowsPackage -Path "$MountDirectory"
    $GetWindowsPackage | Out-File "$Info\Get-WindowsPackage.txt"
    $GetWindowsPackage | Out-File "$OutPath\WindowsPackage.txt"
    $GetWindowsPackage | Out-File "$Logs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Get-WindowsPackage.txt"
    $GetWindowsPackage | Export-Clixml -Path "$LogsXML\Get-WindowsPackage.xml"
    $GetWindowsPackage | Export-Clixml -Path "$LogsXML\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Get-WindowsPackage.xml"
    $GetWindowsPackage | ConvertTo-Json | Out-File "$LogsJS\Get-WindowsPackage.json"
    $GetWindowsPackage | ConvertTo-Json | Out-File "$LogsJS\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Get-WindowsPackage.json"
}