function Write-Transcript
{
    [CmdletBinding()]
    Param(
        [parameter(Mandatory=$true)]
        $WindowsImage
    )

    #$OSImagePath = $($WindowsImage.ImagePath)
    #$OSImageIndex = $($WindowsImage.ImageIndex)
    $OSSourcePath = Split-Path -Path "$OSImagePath" -Qualifier
    #$WindowsImage = Get-WindowsImage -ImagePath "$OSImagePath" -Index $OSImageIndex | Select-Object -Property *

    $ImageName = Format-WindowsImageName $($WindowsImage.ImageName)

    # $OSImageDescription = $($WindowsImage.ImageDescription)
    $OSArchitecture = Format-WindowsImageArchitecture $($WindowsImage.Architecture)

    #======================================================================================
    #	Start the Transcript 18.9.13
    #======================================================================================
    $ScriptName = $MyInvocation.MyCommand.Name
    $LogName = "$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-$ScriptName.log"
    # Start-Transcript -Path (Join-Path $Logs $LogName)
    Write-Host "===========================================================================" -ForegroundColor Yellow
    Write-Host "OSMedia Information" -ForegroundColor Yellow
    Write-Host "===========================================================================" -ForegroundColor Yellow
    Write-Host "Source Path:		$OSSourcePath" -ForegroundColor Yellow
    Write-Host "-Image File:		$($WindowsImage.ImagePath)" -ForegroundColor Cyan
    Write-Host "-Image Index:		$($WindowsImage.ImageIndex)" -ForegroundColor Cyan
    Write-Host "-Name:				$ImageName" -ForegroundColor Cyan
    Write-Host "-Description:		$($WindowsImage.ImageDescription)" -ForegroundColor Cyan
    Write-Host "-Architecture:		$OSArchitecture" -ForegroundColor Cyan
    Write-Host "-Edition:			$($WindowsImage.EditionId)" -ForegroundColor Cyan
    Write-Host "-Type:				$($WindowsImage.InstallationType)" -ForegroundColor Cyan
    Write-Host "-Languages:			$($WindowsImage.Languages)" -ForegroundColor Cyan
    Write-Host "-Build:				$($WindowsImage.Build)" -ForegroundColor Cyan
    Write-Host "-Version:			$($WindowsImage.Version)" -ForegroundColor Cyan
    Write-Host "-SPBuild:			$($WindowsImage.SPBuild)" -ForegroundColor Cyan
    Write-Host "-SPLevel:			$($WindowsImage.SPLevel)" -ForegroundColor Cyan
    Write-Host "-Bootable:			$($WindowsImage.ImageBootable)" -ForegroundColor Cyan
    Write-Host "-WimBoot:			$($WindowsImage.WIMBoot)" -ForegroundColor Cyan
    Write-Host "-Created Time:		$($WindowsImage.CreatedTime)" -ForegroundColor Cyan
    Write-Host "-Modified Time:		$($WindowsImage.ModifiedTime)" -ForegroundColor Cyan
    # Write-Host "-UBR:				$UBR" -ForegroundColor Cyan
    # Write-Host ""
    # Write-Host "Working Path:		$WorkingPath" -ForegroundColor Yellow
    # Write-Host "-OSMedia Name:		$OSMediaName" -ForegroundColor Cyan
    # Write-Host "-Info:				$Info" -ForegroundColor Cyan
    # Write-Host "-Logs:				$Logs" -ForegroundColor Cyan
    # Write-Host "-OS:				$OS" -ForegroundColor Cyan
    # Write-Host "-WinPE:				$WinPE" -ForegroundColor Cyan
}