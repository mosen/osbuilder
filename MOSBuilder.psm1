#   Reference
#   https://github.com/RamblingCookieMonster/PSStackExchange/blob/master/PSStackExchange/PSStackExchange.psm1

$Public  = @( Get-ChildItem -Path $PSScriptRoot\Public\*.ps1 -ErrorAction SilentlyContinue )
$Private = @( Get-ChildItem -Path $PSScriptRoot\Private\*.ps1 -ErrorAction SilentlyContinue )

foreach ($import in @($Public + $Private)) {
    Try {. $import.fullname}
    Catch {Write-Error -Message "Failed to import function $($import.fullname): $_"}
}

Export-ModuleMember -Function $Public.Basename

# Implement your module commands in this script.

# Export only the functions using PowerShell standard verb-noun naming.
# Be sure to list each exported functions in the FunctionsToExport field of the module manifest file.
# This improves performance of command discovery in PowerShell.
#Export-ModuleMember -Function *-*

<# . $PSScriptRoot\Get-OSBuilder.ps1
. $PSScriptRoot\Get-OSBuilderUpdates.ps1
. $PSScriptRoot\Import-OSMedia.ps1
. $PSScriptRoot\New-OSBuild.ps1
. $PSScriptRoot\New-OSBuildTask.ps1
. $PSScriptRoot\Export-BootImageInformation.ps1
. $PSScriptRoot\Export-WindowsImageInformation.ps1
. $PSScriptRoot\Export-WindowsMediaInformation.ps1
. $PSScriptRoot\Format-WindowsImageArchitecture.ps1
. $PSScriptRoot\Format-WindowsInstallMediaName.ps1
. $PSScriptRoot\Get-OSBuilder.ps1
. $PSScriptRoot\Get-OSBuilderUpdates.ps1
. $PSScriptRoot\Import-OSMedia.ps1
. $PSScriptRoot\Import-WindowsMedia.ps1
. $PSScriptRoot\Import-WinREMedia.ps1
. $PSScriptRoot\Invoke-OSBuildTask.ps1
. $PSScriptRoot\New-OSBuilderISO.ps1
. $PSScriptRoot\New-OSBuilderUSB.ps1
. $PSScriptRoot\Show-OSInfo.ps1
. $PSScriptRoot\Update-OSMedia.ps1
. $PSScriptRoot\Test-WindowsInstallMedia.ps1
. $PSScriptRoot\Update-OSMedia.ps1
#>
