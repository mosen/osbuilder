function New-OSBuilderUSB {
    [CmdletBinding()]
    Param (
        [ValidateLength(1,11)]
        [string]$USBLabel
    )
    BEGIN {
        if (-not $PSBoundParameters.ContainsKey('Confirm')) {$ConfirmPreference = $PSCmdlet.SessionState.PSVariable.GetValue('ConfirmPreference')}
        if (-not $PSBoundParameters.ContainsKey('WhatIf')) {$WhatIfPreference = $PSCmdlet.SessionState.PSVariable.GetValue('WhatIfPreference')}
    }

    PROCESS {
        #======================================================================================
        # Validate Administrator Rights
        #======================================================================================
        if (!([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole(`
             [Security.Principal.WindowsBuiltInRole] "Administrator"))
        {
            Write-Host ""
            Write-Host "OSBuilder: This function needs to be run as Administrator" -ForegroundColor Yellow
            Write-Host ""
            Return
        }
        #======================================================================================
        # Get-OSBuilder
        #======================================================================================
        Get-OSBuilder -HideDetails
        Write-Host ""
        #======================================================================================
        # Validate OSMedia and OSBuilds has content
        #======================================================================================
        $SelectedOSMedia = Get-ChildItem -Path ("$OSBuilderOSMedia","$OSBuilderOSBuilds") -Directory | Select-Object -Property Name, FullName
        if ($null -eq $SelectedOSMedia) {
            Write-Warning "OSBuilder: OSMedia or OSBuilds content not found.  Use Import-OSMedia to import an Operating System first . . . Exiting!"
            Break
        }
        #======================================================================================
        # Validate OSMedia has an install.wim
        #======================================================================================
        $SelectedOSMedia = $SelectedOSMedia | Where-Object {Test-Path $(Join-Path $_.FullName (Join-Path "OS" (Join-Path "sources" "install.wim")))}
        if ($null -eq $SelectedOSMedia) {
            Write-Warning "OSBuilder: OSMedia or OSBuilds Install.wim not found.  Use Import-OSMedia to import an Operating System first . . . Exiting!"
            Break
        }
        #======================================================================================
        # Validate OSMedia was imported with Import-OSMedia
        #======================================================================================
        $SelectedOSMedia = $SelectedOSMedia | Where-Object {Test-Path $(Join-Path $_.FullName "WindowsImage.txt")}
        if ($null -eq $SelectedOSMedia) {
            Write-Warning "OSBuilder: OSMedia or OSBuilds content is invalid (missing WindowsImage.txt).  Use Import-OSMedia to import an Operating System first . . . Exiting!"
            Return
        }
        #======================================================================================
        # Select Source OSMedia
        #======================================================================================
        $SelectedOSMedia = $SelectedOSMedia | Out-GridView -Title "OSBuilder: Select an OSMedia or OSBuild to copy to the USB Drive and press OK (Cancel to Exit)" -OutputMode Single
        if($null -eq $SelectedOSMedia) {
            Write-Warning "OSBuilder: Source OSMedia or OSBuild was not selected . . . Exiting!"
            Return
        }
        #======================================================================================
        # Select USB Drive
        #======================================================================================
        $Results = Get-Disk | Where-Object {$_.Size/1GB -lt 33 -and $_.BusType -eq 'USB'} | Out-GridView -Title 'OSBuilder: Select a USB Drive to FORMAT' -OutputMode Single | Clear-Disk -RemoveData -RemoveOEM -Confirm:$false -PassThru | New-Partition -UseMaximumSize -IsActive -AssignDriveLetter | Format-Volume -FileSystem FAT32 -NewFileSystemLabel $USBLabel

        if ($null -eq $Results) {
            Write-Warning "OSBuilder: No USB Drive was Found or Selected"
            Return
        } else {
            #Make Bootable
            Set-Location -Path "$($SelectedOSMedia.FullName)\OS\boot"
            bootsect.exe /nt60 "$($Results.DriveLetter):"

            #Copy Files from ISO to USB
            Copy-Item -Path "$($SelectedOSMedia.FullName)\OS\*" -Destination "$($Results.DriveLetter):" -Recurse -Verbose
        }
    }

    END {
        #======================================================================================
        Write-Host "===========================================================================" -ForegroundColor Green
        Write-Host "Complete!" -ForegroundColor Green
        Write-Host "===========================================================================" -ForegroundColor Green
        #======================================================================================
    }
}