function Update-OSMedia {
    [CmdletBinding()]
    Param (
        [scriptblock]$Filter = {$true},
        [string]$ByName,
        [switch]$CustomCumulativeUpdate,
        [switch]$CustomServicingStack,
        [switch]$DownloadUpdates,
        [switch]$Execute
    )
    Write-Host "===========================================================================" -ForegroundColor Green
    Write-Host "Update-OSMedia" -ForegroundColor Green
    Write-Host "===========================================================================" -ForegroundColor Green
    
    Test-AdminRights

    #======================================================================================
    #   Initialize OSBuilder 18.9.24
    #======================================================================================
    Get-OSBuilder -CreatePaths -HideDetails

    #$ConfigPath = $(Join-Path -Path $PSScriptRoot -ChildPath "..\OSBuilderConfigurationData.psd1")
    #$config = Import-PowerShellDataFile -Path $ConfigPath
    #$config | Format-List
    #Return

    # Select Source OSMedia 18.10.3
    $AllOSMedia = Get-ChildItem -Path "$OSBuilderOSMedia" -Directory | Where-Object { Test-OSMedia $(Join-Path $OSBuilderOSMedia $_) }
    $AllOSMedia = $AllOSMedia | Where-Object $Filter

    if ($null -eq $AllOSMedia) {
        Write-Warning "Could not find a matching OSMedia to update . . . Exiting!"
        Break
    }

    if ($AllOSMedia.Count -gt 1) {
        Write-Warning "Updating Multiple Operating Systems.  This may take some time ..."
    }

    # NOT IMPLEMENTED: Select Custom Servicing Stack 18.10.2
    # NOT IMPLEMENTED: Select Custom Cumulative Update 18.10.2

    #======================================================================================
    #   Update OSMedia 18.10.3
    #======================================================================================
    foreach ($SelectedOSMedia in $AllOSMedia) {
        #======================================================================================
        #   Get Windows Image Information 18.10.3
        #======================================================================================
        $OSSourcePath = "$($SelectedOSMedia.FullName)"
        $OSImagePath = "$OSSourcePath\OS\sources\install.wim"
        $OSImageIndex = 1
        $WindowsImage = Get-WindowsImage -ImagePath "$OSImagePath" -Index $OSImageIndex | Select-Object -Property *

        Write-OSMediaInformation -WindowsImage $WindowsImage
        $OSArchitecture = Format-WindowsImageArchitecture $($WindowsImage.Architecture)
        $OSBuild = $($WindowsImage.Build)

        if (Test-Path "$OSSourcePath\info\xml\CurrentVersion.xml") {
            $RegCurrentVersion = Import-Clixml -Path "$OSSourcePath\info\xml\CurrentVersion.xml"
            $OSVersionNumber = $($RegCurrentVersion.ReleaseId)            
            if ($OSVersionNumber -gt 1803) {
                Write-Warning "OSBuilder does not currently support this version of Windows ... Check for an updated version"
                Write-Warning "OSBuilder cannot proceed . . . Exiting"
                Return
            }
        } else {
            if ($OSBuild -eq 10240) {$OSVersionNumber = 1507}
            if ($OSBuild -eq 14393) {$OSVersionNumber = 1607}
            if ($OSBuild -eq 15063) {$OSVersionNumber = 1703}
            if ($OSBuild -eq 16299) {$OSVersionNumber = 1709}
            if ($OSBuild -eq 17134) {$OSVersionNumber = 1803}
        }
        #======================================================================================
        #   Set Working Path 18.9.24
        #======================================================================================
        $BuildName = "build$((Get-Date).ToString('mmss'))"
        $WorkingPath = "$OSBuilderOSMedia\$BuildName"
        #======================================================================================
        #   Validate Exiting WorkingPath 18.9.24
        #======================================================================================
        if (Test-Path $WorkingPath) {
            Write-Warning "$WorkingPath exists.  Contents will be replaced"
            Remove-Item -Path "$WorkingPath" -Force -Recurse
            Write-Host ""
        }
        #======================================================================================
        #   Update Catalogs 18.9.23
        #======================================================================================
        if (!(Test-Path $CatalogLocal)) {Get-OSBuilderUpdates -UpdateCatalogs -HideDetails}
        #======================================================================================
        #   Get Catalogs 18.9.23
        #======================================================================================
        $ImportCatalog = @()
        $CatalogDownloads = @()
        $CatalogsXmls = Get-ChildItem "$OSBuilderContent\Updates" *.xml -Recurse
        foreach ($CatalogsXml in $CatalogsXmls) {
            $ImportCatalog = Import-Clixml -Path "$($CatalogsXml.FullName)"
            $CatalogDownloads += $ImportCatalog
        }
        #======================================================================================
        #   Adobe Updates 18.9.23
        #======================================================================================
        $UpdateCatAdobe = $CatalogDownloads | Where-Object {
            Test-CatalogUpdate -CatalogUpdate $_ -Category 'Adobe' -OSVersionNumber $OSVersionNumber -OSArchitecture $OSArchitecture -OSInstallationType $OSInstallationType
        }
        #======================================================================================
        #   Component Updates 18.9.23
        #======================================================================================
        $UpdateCatComponent = $CatalogDownloads | Where-Object {
            Test-CatalogUpdate -CatalogUpdate $_ -Category 'Component' -OSVersionNumber $OSVersionNumber -OSArchitecture $OSArchitecture -OSInstallationType $OSInstallationType
        }
        $UpdateCatComponent = $UpdateCatComponent | Sort-Object -Property KBTitle
        #======================================================================================
        #   Cumulative Updates 18.10.2
        #======================================================================================
        $UpdateCatCumulative = $CatalogDownloads | Where-Object {
            Test-CatalogUpdate -CatalogUpdate $_ -Category 'Cumulative' -OSVersionNumber $OSVersionNumber -OSArchitecture $OSArchitecture -OSInstallationType $OSInstallationType
        }
        $UpdateCatCumulative = $UpdateCatCumulative | Sort-Object -Property DatePosted
        #======================================================================================
        #   Servicing Stacks 18.9.23
        #======================================================================================
        $UpdateCatServicing = $CatalogDownloads | Where-Object {
            Test-CatalogUpdate -CatalogUpdate $_ -Category 'Servicing' -OSVersionNumber $OSVersionNumber -OSArchitecture $OSArchitecture -OSInstallationType $OSInstallationType
        }
        #======================================================================================
        #   Setup Updates 18.9.23
        #======================================================================================
        $UpdateCatSetup = $CatalogDownloads | Where-Object {
            Test-CatalogUpdate -CatalogUpdate $_ -Category 'Setup' -OSVersionNumber $OSVersionNumber -OSArchitecture $OSArchitecture -OSInstallationType $OSInstallationType
        }
        #======================================================================================
        #   Update Validation 18.9.24
        #======================================================================================
        Write-Host "===========================================================================" -ForegroundColor Yellow
        Write-Host "Updates to Apply" -ForegroundColor Yellow
        if ($DownloadUpdates.IsPresent) {Get-OSBuilderUpdates -UpdateCatalogs -HideDetails}

        # Download Adobe Updates (or just print)
        Download-Updates -UpdateCatalog $UpdateCatAdobe -UpdateCachePath "$OSBuilderContent\Updates" -DownloadUpdates $DownloadUpdates
        # Component Updates
        Download-Updates -UpdateCatalog $UpdateCatComponent -UpdateCachePath "$OSBuilderContent\Updates" -DownloadUpdates $DownloadUpdates

        if ($CustomCumulativeUpdate.IsPresent) {
            Write-Host "$($UpdateCatCumulative.FullName)"
        } else {
            # Cumulative Updates
            Download-Updates -UpdateCatalog $UpdateCatCumulative -UpdateCachePath "$OSBuilderContent\Updates" -DownloadUpdates $DownloadUpdates
        }
        if ($CustomServicingStack.IsPresent) {
            Write-Host "$($UpdateCatServicing.FullName)"
        } else {
            # Servicing Stack Updates
            Download-Updates -UpdateCatalog $UpdateCatServicing -UpdateCachePath "$OSBuilderContent\Updates" -DownloadUpdates $DownloadUpdates
        }

        # Setup Updates
        Download-Updates -UpdateCatalog $UpdateCatSetup -UpdateCachePath "$OSBuilderContent\Updates" -DownloadUpdates $DownloadUpdates

        #======================================================================================
        #   Execute 18.9.24
        #======================================================================================
        if ($Execute.IsPresent) {
            $Info = Join-Path $WorkingPath 'info'
            $LogsJS = Join-Path $Info 'json'
            $LogsXML = Join-Path $Info 'xml'
            $Logs =	Join-Path $Info "logs"
            if (!(Test-Path "$Info"))		{New-Item "$Info" -ItemType Directory -Force | Out-Null}
            if (!(Test-Path "$LogsJS"))		{New-Item "$LogsJS" -ItemType Directory -Force | Out-Null}
            if (!(Test-Path "$LogsXML"))	{New-Item "$LogsXML" -ItemType Directory -Force | Out-Null}
            if (!(Test-Path "$Logs"))		{New-Item "$Logs" -ItemType Directory -Force | Out-Null}

            $OS = Join-Path $WorkingPath "OS"
            $WinPE = Join-Path $WorkingPath "WinPE"
            if (!(Test-Path "$OS"))			{New-Item "$OS" -ItemType Directory -Force | Out-Null}
            if (!(Test-Path "$WinPE"))		{New-Item "$WinPE" -ItemType Directory -Force | Out-Null}

            $PEInfo = Join-Path $WinPE 'info'
            $PELogsJS = Join-Path $PEInfo 'json'
            $PELogsXML = Join-Path $PEInfo 'xml'
            $PELogs =	Join-Path $PEInfo "logs"

            if (!(Test-Path "$PEInfo"))		{New-Item "$PEInfo" -ItemType Directory -Force | Out-Null}
            if (!(Test-Path "$PELogsJS"))	{New-Item "$PELogsJS" -ItemType Directory -Force | Out-Null}
            if (!(Test-Path "$PELogsXML"))	{New-Item "$PELogsXML" -ItemType Directory -Force | Out-Null}
            if (!(Test-Path "$PELogs"))		{New-Item "$PELogs" -ItemType Directory -Force | Out-Null}

            $WimTemp = Join-Path $WorkingPath "WimTemp"
            if (!(Test-Path "$WimTemp"))	{New-Item "$WimTemp" -ItemType Directory -Force | Out-Null}
            #======================================================================================
            #   Start the Transcript 18.9.24
            #======================================================================================
            Write-Host "===========================================================================" -ForegroundColor Yellow
            Write-Host "Starting Transcript" -ForegroundColor Yellow
            $ScriptName = $MyInvocation.MyCommand.Name
            $LogName = "$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-$ScriptName.log"
            Start-Transcript -Path (Join-Path $Logs $LogName)
            #======================================================================================
            #   Display Build Paths 18.9.24
            #======================================================================================
            Write-Host "===========================================================================" -ForegroundColor Yellow
            Write-Host "Creating $BuildName" -ForegroundColor Yellow
            Write-Host "Working Path:       $WorkingPath" -ForegroundColor Yellow
            Write-Host "-Info:              $Info" -ForegroundColor Cyan
            Write-Host "-Logs:              $Logs" -ForegroundColor Cyan
            Write-Host "-OS:                $OS" -ForegroundColor Cyan
            Write-Host "-WinPE:             $WinPE" -ForegroundColor Cyan
            #======================================================================================
            #   Create Mount Directories 18.9.10
            #======================================================================================
            $MountDirectory = Join-Path $OSBuilderContent\Mount "os$((Get-Date).ToString('mmss'))"
            if ( ! (Test-Path "$MountDirectory")) {New-Item "$MountDirectory" -ItemType Directory -Force | Out-Null}
            #======================================================================================
            #	Copy OS 18.10.3
            #======================================================================================
            Write-Host "===========================================================================" -ForegroundColor Yellow
            Write-Host "Copy Operating System to $WorkingPath" -ForegroundColor Yellow
            Copy-Item -Path "$OSSourcePath\*" -Destination "$WorkingPath" -Exclude ('*.wim','*.iso') -Recurse -Force | Out-Null
            if (Test-Path "$WorkingPath\ISO") {Remove-Item -Path "$WorkingPath\ISO" -Force -Recurse | Out-Null}
            Copy-Item -Path "$OSSourcePath\OS\sources\install.wim" -Destination "$WimTemp\install.wim" -Force | Out-Null
            Copy-Item -Path "$OSSourcePath\WinPE\*.wim" -Destination "$WimTemp" -Exclude boot.wim -Force | Out-Null
            #======================================================================================
            #   Setup Update 18.9.24
            #======================================================================================
            Write-Host "===========================================================================" -ForegroundColor Yellow
            Write-Host "Setup Update" -ForegroundColor Yellow
            if (!($null -eq $UpdateCatSetup)) {
                foreach ($Update in $UpdateCatSetup) {
                    $UpdateCatSetup = $(Get-ChildItem -Path $OSBuilderContent\Updates -File -Recurse | Where-Object {$_.Name -eq $($Update.FileName)}).FullName
                    if (Test-Path "$UpdateCatSetup") {
                        expand.exe "$UpdateCatSetup" -F:*.* "$OS\Sources"
                    } else {
                        Write-Warning "Not Found: $UpdateCatSetup"
                    }
                }
            }

            $SetupWIMParams = @{
                ImagePath = "$WimTemp\setup.wim";
                OSBuilderContent = "$OSBuilderContent";
                LogPath = $PELogs;
                CustomCumulativeUpdate = $CustomCumulativeUpdate;
                CustomServicingStack = $CustomServicingStack;
                UpdateCatServicing = $UpdateCatServicing;
                UpdateCatCumulative = $UpdateCatCumulative;
            }
            Update-SetupWIM @SetupWIMParams

            $WinPEWIMParams = @{
                ImagePath = "$WimTemp\winpe.wim";
                OSBuilderContent = "$OSBuilderContent";
                LogPath = $PELogs;
                CustomCumulativeUpdate = $CustomCumulativeUpdate;
                CustomServicingStack = $CustomServicingStack;
                UpdateCatServicing = $UpdateCatServicing;
                UpdateCatCumulative = $UpdateCatCumulative;
            }
            Update-WinPEWIM @WinPEWIMParams

            $WinREWIMParams = @{
                ImagePath = "$WimTemp\winre.wim";
                OSBuilderContent = "$OSBuilderContent";
                LogPath = $PELogs;
                CustomCumulativeUpdate = $CustomCumulativeUpdate;
                CustomServicingStack = $CustomServicingStack;
                UpdateCatServicing = $UpdateCatServicing;
                UpdateCatCumulative = $UpdateCatCumulative;
            }
            Update-WinREWIM @WinREWIMParams

            Write-Host "BREAK"
            Return

            #======================================================================================
            #   Rebuild Boot.wim 18.9.10
            #======================================================================================
            Write-Host "===========================================================================" -ForegroundColor Yellow
            Write-Host "WinPE Phase: Rebuilding Boot.wim" -ForegroundColor Yellow
            Write-Host "Rebuilding updated Boot.wim Index 1 at $WinPE\boot.wim"
            Export-WindowsImage -SourceImagePath "$WimTemp\winpe.wim" -SourceIndex 1 -DestinationImagePath "$WinPE\boot.wim" -LogPath "$PELogs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Export-WindowsImage-boot.wim.log" | Out-Null
            Write-Host "Rebuilding updated Boot.wim Index 2 at $WinPE\boot.wim Bootable"
            Export-WindowsImage -SourceImagePath "$WimTemp\setup.wim" -SourceIndex 1 -DestinationImagePath "$WinPE\boot.wim" -Setbootable -LogPath "$PELogs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Export-WindowsImage-boot.wim.log" | Out-Null
            Write-Host "Copying Boot.wim to $OS\sources\boot.wim"
            Copy-Item -Path "$WinPE\boot.wim" -Destination "$OS\sources\boot.wim" -Force | Out-Null


            #======================================================================================
            #   Saving WinPE Image Configuration 18.10.2
            #======================================================================================
            Write-Host "===========================================================================" -ForegroundColor Yellow
            Write-Host "Inventory Phase: Saving WinPE Image Configuration" -ForegroundColor Yellow
            #======================================================================================
            #   Get-WindowsImage Boot.wim 18.10.2
            #======================================================================================
            Export-BootImageInformation -ImagePath "$OS\sources\boot.wim" -OutPath "$WorkingDir"
            
            #======================================================================================
            #   Display OS Information 18.10.2
            #======================================================================================
            Show-OSInfo $WorkingPath
            #======================================================================================
            #   Remove Temporary Files 18.9.13
            #======================================================================================
            if (Test-Path "$WimTemp") {Remove-Item -Path "$WimTemp" -Force -Recurse | Out-Null}
            if (Test-Path "$MountDirectory") {Remove-Item -Path "$MountDirectory" -Force -Recurse | Out-Null}
            
            #======================================================================================
            #   UBR Validation 18.9.12
            #======================================================================================
            if ($UBRPre -eq $UBR) {
                Write-Host "===========================================================================" -ForegroundColor Yellow
                Write-Warning "The Update Build Revision did not change after Windows Updates"
                Write-Warning "There may have been an issue applying the Cumulative Update if this was not expected"
            }
            if (!($UBR)) {$UBR = $((Get-Date).ToString('mmss'))}
            #======================================================================================
            #   Set New Name 18.9.12
            #======================================================================================
            $OSImageName = $($GetWindowsImage.ImageName)
            $OSImageName = $OSImageName -replace "Windows 10", "Win10"
            $OSImageName = $OSImageName -replace "Enterprise", "Ent"
            $OSImageName = $OSImageName -replace "Education", "Edu"
            $OSImageName = $OSImageName -replace " for ", " "
            $OSImageName = $OSImageName -replace "Workstations", "Wks"
            $OSImageName = $OSImageName -replace "Windows Server 2016", "Svr2016"
            $OSImageName = $OSImageName -replace "ServerStandardACore", "Std Core"
            $OSImageName = $OSImageName -replace "ServerDatacenterACore", "DC Core"
            $OSImageName = $OSImageName -replace "ServerStandardCore", "Std Core"
            $OSImageName = $OSImageName -replace "ServerDatacenterCore", "DC Core"
            $OSImageName = $OSImageName -replace "ServerStandard", "Std"
            $OSImageName = $OSImageName -replace "ServerDatacenter", "DC"
            $OSImageName = $OSImageName -replace "Standard", "Std"
            $OSImageName = $OSImageName -replace "Datacenter", "DC"
            $OSImageName = $OSImageName -replace 'Desktop Experience', 'DTE'
            $OSImageName = $OSImageName -replace '\(', ''
            $OSImageName = $OSImageName -replace '\)', ''

            $OSArchitecture = $($GetWindowsImage.Architecture)
            if ($OSArchitecture -eq 0) {$OSArchitecture = 'x86'}
            elseif ($OSArchitecture -eq 1) {$OSArchitecture = 'MIPS'}
            elseif ($OSArchitecture -eq 2) {$OSArchitecture = 'Alpha'}
            elseif ($OSArchitecture -eq 3) {$OSArchitecture = 'PowerPC'}
            elseif ($OSArchitecture -eq 6) {$OSArchitecture = 'ia64'}
            elseif ($OSArchitecture -eq 9) {$OSArchitecture = 'x64'}

            $OSBuild = $($GetWindowsImage.Build)
            $OSVersionNumber = $null
            if (Test-Path "$LogsXML\CurrentVersion.xml") {
                $RegCurrentVersion = Import-Clixml -Path "$LogsXML\CurrentVersion.xml"
                $OSVersionNumber = $($RegCurrentVersion.ReleaseId)
            } else {
                if ($OSBuild -eq 10240) {$OSVersionNumber = 1507}
                if ($OSBuild -eq 14393) {$OSVersionNumber = 1607}
                if ($OSBuild -eq 15063) {$OSVersionNumber = 1703}
                if ($OSBuild -eq 16299) {$OSVersionNumber = 1709}
                if ($OSBuild -eq 17134) {$OSVersionNumber = 1803}
            }

            $OSLanguages = $($GetWindowsImage.Languages)
            if ($null -eq $OSVersionNumber ) {
                Write-Host ""
                Write-Warning "OS Build $OSVersionNumber is not automatically recognized"
                Write-Warning "Check for an updated version of OSBuilder"
                Write-Host ""
                if ($BuildName -like "build*") {$BuildName = "$OSImageName $OSArchitecture"}
            } else {
                if ($BuildName -like "build*") {$BuildName = "$OSImageName $OSArchitecture $OSVersionNumber"}
                
            }
            $BuildName = "$BuildName $OSLanguages"
            if ($($OSLanguages.count) -eq 1) {$BuildName = $BuildName.replace(' en-US', '')}
            
            $NewWorkingPathName = "$BuildName $UBR"
            $NewWorkingPath = "$OSBuilderOSMedia\$NewWorkingPathName"
            #======================================================================================
            #   Rename Build Directory
            #======================================================================================
            if (Test-Path $NewWorkingPath) {
                Write-Host ""
                Write-Warning "Trying to rename the Build directory, but it already exists"
                Write-Warning "Appending the HHmm to the directory name"
                $NewWorkingPathName = "$NewWorkingPathName $((Get-Date).ToString('mmss'))"
            }
            Write-Host "===========================================================================" -ForegroundColor Yellow
            Write-Host "Closing Phase: Renaming ""$WorkingPath"" to ""$NewWorkingPathName""" -ForegroundColor Yellow
            Stop-Transcript
            Rename-Item -Path "$WorkingPath" -NewName "$NewWorkingPathName" -ErrorAction Stop
        }
        Write-Host "===========================================================================" -ForegroundColor Green
        Write-Host "Complete!" -ForegroundColor Green
        Write-Host "===========================================================================" -ForegroundColor Green
    }
}