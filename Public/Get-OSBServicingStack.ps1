function Get-OSBServicingStack {
    <#
    .SYNOPSIS
        List servicing stack updates that have been downloaded by OSBuilder
    #>
    Param(
        [parameter(Mandatory=$true)]
        [string]$Path
    )

    $config = Import-PowerShellDataFile -Path ..\OSBuilderConfigurationData.psd1

    $UpdateCatServicing = Get-ChildItem -Path "$($config.OSBuilderContent)\Updates\Custom" -File -Recurse | Select-Object -Property FullName
    $UpdateCatServicing | Format-Table
}