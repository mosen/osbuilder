
function Import-OSMedia 
{
	<#
	.SYNOPSIS
		Import Operating System Installation Media

	.DESCRIPTION
		Import all Windows installation media from a source directory containing .iso files that matches
		the specified criteria.

		All source media is copied into the specified destination path, and metadata is generated about each of the
		available installation media. Specific indexes are extracted from install.wim and boot.wim and placed into the destination path.

		Parameters are available to filter desired windows editions and architectures from the available media.

	.PARAMETER Editions
		Specify a list of acceptable Windows Editions to import. Only those specified will be imported.

	#>
    [CmdletBinding()]
    Param (
		[parameter(Mandatory=$true, HelpMessage="Enter the full path to a directory containing Windows ISO file(s)")]
		[string] $Source,

		[parameter(HelpMessage="Enter the full path to a destination folder where the installation media will be copied")]
		[string] $Destination = "C:\OSBuilder\OSMedia",

		[parameter()]
		[ValidateSet("All", "S", "SN", "Home", "HomeN", "Education", "EducationN", "Pro", "ProN", "ServerCore", "ServerStandard", "ServerDatacenter")]
		[String[]] $Editions = @("Education")
	)

	Write-Host "===========================================================================" -ForegroundColor Green
	Write-Host "Import-OSMedia" -ForegroundColor Green
	Write-Host "===========================================================================" -ForegroundColor Green

	if (!([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole(`
			[Security.Principal.WindowsBuiltInRole] "Administrator"))
	{
		Write-Host ""
		Write-Host "This function needs to be run as Administrator" -ForegroundColor Yellow
		Write-Host ""
		Return
	}

	Write-Host "Checking source directory ($($Source)) for .iso files containing install.wim" -ForegroundColor Yellow
	$ISOFiles = Get-ChildItem -Path $Source -File -Filter *.iso
	$InstallMedia = $ISOFiles | Where-Object {Test-WindowsInstallMedia (Join-Path $Source $_.Name)}
	if ($InstallMedia.Length -eq 0) { 
		Write-Warning -Message "No suitable installation media found, exiting."
		Return 
	} else {
		Write-Host "Found the following suitable media:" -ForegroundColor Green
		$InstallMedia | ForEach-Object { Write-Host $_ }
	}

	Write-Host "Scanning Image Information ... Please Wait!" -ForegroundColor Yellow
	Write-Host "===========================================================================" -ForegroundColor Yellow
	$MountedImage = Mount-DiskImage -ImagePath $InstallMedia[0].FullName -PassThru

	Try {
		$MountedVolume = $MountedImage | Get-Volume
		
		# Map edition arguments given on command line to EditionId within the images.
		$EditionMap = @{
			"All" = "Professional"; # DERP
			"S" = "Cloud";
			"SN" = "CloudN";
			"Home" = "Core";
			"HomeN" = "CoreN";
			"Education" = "Education";
			"EducationN" = "EducationN";
			"Pro" = "Professional";
			"ProN" = "ProfessionalN";
		}

		# Filter out only desired editions
		$EditionsWanted = $Editions | ForEach-Object { $EditionMap[$_] }
		$script:WindowsImages = [System.Collections.ArrayList]@()
		Get-WindowsImage -ImagePath (Join-Path "$($MountedVolume.DriveLetter):\sources" "install.wim") | ForEach-Object {
			$WindowsImage = Get-WindowsImage -ImagePath "$($_.ImagePath)" -Index $($_.ImageIndex)
			if ($EditionsWanted.Contains($WindowsImage.EditionId)) {
				$script:WindowsImages += $WindowsImage
			}
		}

		$MountPath = "C:\OSBuilder\Content\Mount"

		# Each item is a [Microsoft.Dism.Commands.ImageInfoObject]
		foreach ($WindowsImage in $WindowsImages) {
			
			Write-Host "Mounting Install.wim" -ForegroundColor Yellow
			$MountDirectory = Join-Path $MountPath "os$((Get-Date).ToString('HHmmss'))"
			if ( ! (Test-Path "$MountDirectory")) {New-Item "$MountDirectory" -ItemType Directory -Force | Out-Null}
			# https://docs.microsoft.com/en-us/powershell/module/dism/mount-windowsimage?view=win10-ps

			Mount-WindowsImage -ImagePath "$($WindowsImage.ImagePath)" -Index $WindowsImage.ImageIndex -Path "$MountDirectory" -Optimize -ReadOnly

			Try {
				Write-Verbose -Message "Loading Registry offline to retrieve OS Version and UBR"
				reg LOAD 'HKLM\OSMedia' "$MountDirectory\Windows\System32\Config\SOFTWARE"
				$RegCurrentVersion = Get-ItemProperty -Path 'HKLM:\OSMedia\Microsoft\Windows NT\CurrentVersion'
				reg UNLOAD 'HKLM\OSMedia'

				$OSMediaName = Format-WindowsInstallMediaName -WindowsImage $WindowsImage -RegCurrentVersion $RegCurrentVersion
				Write-Host "Using OSMediaName $($OSMediaName)"
				
				$WorkingPath = Join-Path $Destination $OSMediaName
				# if (Test-Path $WorkingPath) {
				# 	Write-Warning "$WorkingPath exists.  Contents will be replaced!"
				# 	Remove-Item -Path "$WorkingPath" -Force -Recurse
				# 	Write-Host ""
				# }

				Export-WindowsMediaInformation -MountDirectory $MountDirectory -OutPath $WorkingPath
				Import-WinREMedia -MountDirectory $MountDirectory -OutPath $WorkingPath
			} Finally {
				Write-Host "===========================================================================" -ForegroundColor Yellow
				Write-Host "Dismounting Install.wim from $MountDirectory" -ForegroundColor Yellow
				Dismount-WindowsImage -Discard -Path "$MountDirectory" -LogPath "$Logs\$((Get-Date).ToString('yyyy-MM-dd-HHmmss'))-Dismount-WindowsImage.log"
			}
			
			Import-WindowsMedia -ImagePath "$($WindowsImage.ImagePath)" -ImageIndex $WindowsImage.ImageIndex -Destination $WorkingPath
			Export-WindowsImageInformation -WindowsImage $WindowsImage -OutPath $WorkingPath
			Export-BootImageInformation -ImagePath "$($WorkingPath)\OS\sources\boot.wim" -OutPath $WorkingPath
			Export-WinREImageInformation -ImagePath "$($WorkingPath)\WinPE\winre.wim" -OutPath $WorkingPath
		}
	} Catch {
		$_.exception.message
	} Finally {
		Write-Host "Unmounting Disk Image (ISO)"
		Dismount-DiskImage -ImagePath $InstallMedia[0].FullName 
	} 

	if (Test-Path "$MountDirectory") {Remove-Item "$MountDirectory" -Force -Recurse | Out-Null}

	Write-Host "===========================================================================" -ForegroundColor Green
	Write-Host "Complete!" -ForegroundColor Green
	Write-Host "===========================================================================" -ForegroundColor Green
}