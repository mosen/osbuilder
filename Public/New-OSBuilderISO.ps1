function New-OSBuilderISO {
    [CmdletBinding()]
    Param ()
    BEGIN {
        if (-not $PSBoundParameters.ContainsKey('Confirm')) {$ConfirmPreference = $PSCmdlet.SessionState.PSVariable.GetValue('ConfirmPreference')}
        if (-not $PSBoundParameters.ContainsKey('WhatIf')) {$WhatIfPreference = $PSCmdlet.SessionState.PSVariable.GetValue('WhatIfPreference')}
    }
    PROCESS {
        Get-OSBuilder -CreatePaths -HideDetails
        Write-Host ""
        #======================================================================================
        # Locate OSCDIMG
        #======================================================================================
        if ($env:PROCESSOR_ARCHITECTURE -eq 'AMD64') {
            if (Test-Path "${env:ProgramFiles(x86)}\Windows Kits\10\Assessment and Deployment Kit\Deployment Tools\amd64\Oscdimg\oscdimg.exe"){
                $oscdimg = "${env:ProgramFiles(x86)}\Windows Kits\10\Assessment and Deployment Kit\Deployment Tools\amd64\Oscdimg\oscdimg.exe"
            } else {
                Write-Warning "Could not locate OSCDIMG"
                Write-Warning "${env:ProgramFiles(x86)}\Windows Kits\10\Assessment and Deployment Kit\Deployment Tools\amd64\Oscdimg\oscdimg.exe"
                Write-Warning "Make sure you have Windows ADK installed before using New-OSBuilderISO"
                Return
            }
        } else {
            Write-Warning "New-OSBuilderISO requires Windows 10 x64"
            Return
        }
        #======================================================================================
        # Validate OSMedia and OSBuilds has content
        #======================================================================================
        $SelectedOSMedia = Get-ChildItem -Path ("$OSBuilderOSBuilds","$OSBuilderOSMedia") -Directory | Select-Object -Property Parent, Name, FullName, CreationTime
        if ($null -eq $SelectedOSMedia) {
            Write-Warning "OSMedia or OSBuilds  content not found.  Use Import-OSMedia to import an Operating System first . . . Exiting!"
            Break
        }
        #======================================================================================
        # Validate OSMedia has an install.wim
        #======================================================================================
        $SelectedOSMedia = $SelectedOSMedia | Where-Object {Test-Path $(Join-Path $_.FullName (Join-Path "OS" (Join-Path "sources" "install.wim")))}
        if ($null -eq $SelectedOSMedia) {
            Write-Warning "OSMedia or OSBuilds Install.wim not found.  Use Import-OSMedia to import an Operating System first . . . Exiting!"
            Break
        }
        #======================================================================================
        # Validate OSMedia was imported with Import-OSMedia
        #======================================================================================
        $SelectedOSMedia = $SelectedOSMedia | Where-Object {Test-Path $(Join-Path $_.FullName "WindowsImage.txt")}
        if ($null -eq $SelectedOSMedia) {
            Write-Warning "OSMedia or OSBuilds content invalid (missing WindowsImage.txt).  Use Import-OSMedia to import an Operating System first . . . Exiting!"
            Return
        }
        #======================================================================================
        # Select Source OSMedia
        #======================================================================================
        $SelectedOSMedia = $SelectedOSMedia | Out-GridView -Title "Select one or more OSMedia or OSBuilds to create an ISO's and press OK (Cancel to Exit)" -PassThru
        if($null -eq $SelectedOSMedia) {
            Write-Warning "Source OSMedia or OSBuild was not selected . . . Exiting!"
            Return
        }

        foreach ($Media in $SelectedOSMedia) {
            $ISOSourceFolder = "$($Media.FullName)\OS"
            $ISODestinationFolder = "$($Media.FullName)\ISO"
            if (!(Test-Path $ISODestinationFolder)) {New-Item $ISODestinationFolder -ItemType Directory -Force | Out-Null}
            $ISOFile = "$ISODestinationFolder\$($Media.Name).iso"
            $WindowsImage = Get-Content -Path "$($Media.FullName)\info\json\Get-WindowsImage.json"
            $WindowsImage = $WindowsImage | ConvertFrom-Json

            $OSImageDescription = $($WindowsImage.ImageName)
            $OSArchitecture = $($WindowsImage.Architecture)
            if ($OSArchitecture -eq 0) {$OSArchitecture = 'x86'}
            elseif ($OSArchitecture -eq 1) {$OSArchitecture = 'MIPS'}
            elseif ($OSArchitecture -eq 2) {$OSArchitecture = 'Alpha'}
            elseif ($OSArchitecture -eq 3) {$OSArchitecture = 'PowerPC'}
            elseif ($OSArchitecture -eq 6) {$OSArchitecture = 'ia64'}
            elseif ($OSArchitecture -eq 9) {$OSArchitecture = 'x64'}
            $UBR = $($WindowsImage.UBR)
        
            $OSImageName = $OSImageName -replace "Windows 10", "Win10"
            $OSImageName = $OSImageName -replace "Enterprise", "Ent"
            $OSImageName = $OSImageName -replace "Education", "Edu"
            $OSImageName = $OSImageName -replace " for ", " "
            $OSImageName = $OSImageName -replace "Workstations", "Wks"
            $OSImageName = $OSImageName -replace "Windows Server 2016", "Svr2016"
            $OSImageName = $OSImageName -replace "ServerStandardACore", "Std Core"
            $OSImageName = $OSImageName -replace "ServerDatacenterACore", "DC Core"
            $OSImageName = $OSImageName -replace "ServerStandardCore", "Std Core"
            $OSImageName = $OSImageName -replace "ServerDatacenterCore", "DC Core"
            $OSImageName = $OSImageName -replace "ServerStandard", "Std"
            $OSImageName = $OSImageName -replace "ServerDatacenter", "DC"
            $OSImageName = $OSImageName -replace "Standard", "Std"
            $OSImageName = $OSImageName -replace "Datacenter", "DC"
            $OSImageName = $OSImageName -replace 'Desktop Experience', 'DTE'
            $OSImageName = $OSImageName -replace '\(', ''
            $OSImageName = $OSImageName -replace '\)', ''

            # 32 character limit for a Label
            # 23 = Win10 Edu x64 17134.112
            # 25 = Win10 Edu N x64 17134.112
            # 23 = Win10 Ent x64 17134.112
            # 25 = Win10 Ent N x64 17134.112
            # 23 = Win10 Pro x64 17134.112
            # 27 = Win10 Pro Edu x64 17134.112
            # 29 = Win10 Pro EduN x64 17134.112
            # 27 = Win10 Pro Wks x64 17134.112
            # 26 = Win10 Pro N x64 17134.112
            # 29 = Win10 Pro N Wks x64 17134.112
            $ISOLabel = '-l"{0}"' -f $OSImageName
            $ISOFolder = "$($Media.FullName)\ISO"
            if (!(Test-Path $ISOFolder)) {New-Item -Path $ISOFolder -ItemType Directory -Force | Out-Null}

            if (!(Test-Path $ISOSourceFolder)) {
                Write-Warning "Could not locate $ISOSourceFolder"
                Write-Warning "Make sure you have proper OS before using New-OSBuilderISO"
                Return
            }
            $etfsboot = "$ISOSourceFolder\boot\etfsboot.com"
            if (!(Test-Path $etfsboot)) {
                Write-Warning "Could not locate $etfsboot"
                Write-Warning "Make sure you have proper OS before using New-OSBuilderISO"
                Return
            }
            $efisys = "$ISOSourceFolder\efi\microsoft\boot\efisys.bin"
            if (!(Test-Path $efisys)) {
                Write-Warning "Could not locate $efisys"
                Write-Warning "Make sure you have proper OS before using New-OSBuilderISO"
                Return
            }
            Write-Host "Label: $OSImageName" -ForegroundColor Yellow
            Write-Host "Creating: $ISOFile" -ForegroundColor Yellow
            $data = '2#p0,e,b"{0}"#pEF,e,b"{1}"' -f $etfsboot, $efisys
            start-process $oscdimg -args @("-m","-o","-u2","-bootdata:$data",'-u2','-udfver102',$ISOLabel,"`"$ISOSourceFolder`"", "`"$ISOFile`"") -Wait
        }
    }
    END {
        #======================================================================================
        Write-Host "===========================================================================" -ForegroundColor Green
        Write-Host "Complete!" -ForegroundColor Green
        Write-Host "===========================================================================" -ForegroundColor Green
        #======================================================================================
    }
}