Describe "Get-OSBuilderUpdates" {
    $Params = @{
        OSBuilderContent = "TestDrive:\OSBuilderContent";
        CatalogLocal = "TestDrive:\OSBuilderContent\Updates\Cat.json"
        OSBuilderCatalogURL = "https://raw.githubusercontent.com/OSDeploy/OSBuilder.Public/master/Content/Updates/Cat.json";
    }
    New-Item -Path $Params.OSBuilderContent -ItemType Directory
    New-Item -Path (Join-Path $Params.OSBuilderContent "Updates")

    It "downloads the update catalog" {
        Get-OSBuilderUpdates @Params 
    }

    It "creates a catalog directory for each catalog" {
        # $OSBuilderContent\Updates\CatalogName
    }

    

}